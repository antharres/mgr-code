#!/bin/bash

trap "exit" INT

n=1

function run_n_times {
    for i in $(eval echo {1..$1})
    do
        echo "`date` Executing $1 times query $2 with $3 workers, iteration $i"
        python -m suite.runner "$2" $3 &>/dev/null
    done
}

workers=1

#set -v

for query in 'startup' 'facebook' 'użytkowni' 'zespół' 'projekt' 'firm' 'aplikac'
do
    echo "------------------------------------------ $query ------------------------------------------"
    # run_n_times ${n} ${query} ${workers}
    echo '--------------------------------------------------------------------------------------------'
done

workers=4
for query in 'facebook'
do
    echo "------------------------------------------ $query ------------------------------------------"
    run_n_times ${n} ${query} ${workers}
    echo '--------------------------------------------------------------------------------------------'
done
