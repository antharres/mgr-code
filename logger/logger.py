from datetime import datetime
import functools
import logging
import os
import time

from suite.config import Configurations


class LogWith(object):
    """
        Logging decorator that allows you to log with a specific logger.
    """

    _MESSAGE_EMPTY = u''
    _MESSAGE_ENTRY = u'Entering {:15s}{}'
    _MESSAGE_EXIT = u'Exiting {:15s} {:5.5f} sec{}'

    LOG_LEVEL_TRACE = 5
    LOG_LEVEL_TRACE_NAME = u'TRACE'
    LOG_LEVEL_FUNC_CALL = 2
    LOG_LEVEL_FUNC_CALL_NAME = u'CALL'

    def __init__(self, logger, log_args=False, log_result=False):
        self._logger = logger
        self._log_args = log_args
        self._log_result = log_result

    def _setup_logger_if_necessary(self, module):
        if not self._logger:
            logging.basicConfig()
            self._logger = logging.getLogger(module)

    def __call__(self, func):
        """
            Returns a wrapper that wraps func.
            The wrapper will log the entry and exit points of the function with logging.INFO level.
        """
        self._setup_logger_if_necessary(func.__module__)

        def _log_entry(func_name, args, kwds):
            args_str = u'(*args: {} **kwds: {})'.format(str(args), str(kwds)) \
                if self._log_args else LogWith._MESSAGE_EMPTY
            self._logger.func_call(self._MESSAGE_ENTRY.format(func_name, args_str))

        def _log_exit(func_name, f_result, time_diff):
            result_str = u'->{}'.format(str(f_result)) \
                if self._log_result else LogWith._MESSAGE_EMPTY
            self._logger.func_call(self._MESSAGE_EXIT.format(func_name, time_diff, result_str))

        def _do_call(*args, **kwds):
            t_start = time.time()

            f_result = func(*args, **kwds)

            t_end = time.time()
            time_diff = t_end - t_start
            return f_result, time_diff

        @functools.wraps(func)
        def wrapper(*args, **kwds):
            _log_entry(func.__name__, args, kwds)

            f_result, time_diff = _do_call(*args, **kwds)

            _log_exit(func.__name__, f_result, time_diff)
            return f_result

        return wrapper


logger_setup_finished = False


def get_logger():
    def get_main_file_name():
        import sys

        return os.path.splitext(os.path.split(sys.modules['__main__'].__file__)[1])[0]

    def setup_logger(_logger):
        def _setup_handlers(logger):
            # global data_path_prefix, log_formatter, root_logger, all_handler, warn_handler, console_handler
            logger.setLevel(LogWith.LOG_LEVEL_FUNC_CALL)
            log_formatter = logging.Formatter(
                u'%(asctime)s %(filename)-12s:%(lineno)d [%(levelname)-5.5s]  %(message)s')

            main_file_name = get_main_file_name()
            config = Configurations.DEFAULT
            if main_file_name == u'runner':
                timestamp = u'_' + datetime.now().strftime("%Y%b%d_%H%M%S")
            else:
                timestamp = u''
            log_directory = u'./{}{}'.format(config.output_dir_path, main_file_name)

            if not os.path.exists(log_directory):
                os.makedirs(log_directory)

            all_file_name = u'{0}/{1}{2}.log'.format(log_directory, u'all', timestamp)
            all_handler = logging.FileHandler(all_file_name, mode=u'w')
            all_handler.setFormatter(log_formatter)
            logger.addHandler(all_handler)

            warn_file_name = u'{0}/{1}{2}.log'.format(log_directory, u'warn', timestamp)
            warn_handler = logging.FileHandler(warn_file_name, mode=u'w')
            warn_handler.setFormatter(log_formatter)
            warn_handler.setLevel(logging.WARN)
            logger.addHandler(warn_handler)

            console_handler = logging.StreamHandler()
            console_handler.setFormatter(log_formatter)
            logger.addHandler(console_handler)

        def _add_trace_and_func_call_logging():
            def trace(self, message, *args, **kws):
                # Yes, logger takes its '*args' as 'args'.
                self._log(LogWith.LOG_LEVEL_TRACE, message, args, **kws)

            def func_call(self, message, *args, **kws):
                # Yes, logger takes its '*args' as 'args'.
                self._log(LogWith.LOG_LEVEL_FUNC_CALL, message, args, **kws)

            logging.Logger.trace = trace
            logging.Logger.func_call = func_call

            logging.addLevelName(LogWith.LOG_LEVEL_TRACE, LogWith.LOG_LEVEL_TRACE_NAME)
            logging.addLevelName(LogWith.LOG_LEVEL_FUNC_CALL, LogWith.LOG_LEVEL_FUNC_CALL_NAME)

        global logger_setup_finished
        if not logger_setup_finished:
            _setup_handlers(_logger)
            _add_trace_and_func_call_logging()
            logger_setup_finished = True

    root_logger = logging.getLogger()
    setup_logger(root_logger)
    return root_logger
