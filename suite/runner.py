import codecs
from codecs import StreamReaderWriter
import gc
import sys

from suite import util
from logger import logger
from logger.logger import LogWith
from suite.crawler.client.retriever import PageRetriever
from suite.crawler.client.worker import Worker
from suite.crawler.server.score.evaluator import Evaluator
from suite.crawler.server.server import Server
from suite.crawler.stats.listeners import CrawlingStatsListener
from suite.model.imported import ImportedCrawlingData
from suite.config import Configurations


root_logger = logger.get_logger()


class Runner(object):
    def __init__(self, workers_count, crawling_data, initial_urls, strategy_class, evaluator, output_file):
        """
        :type output_file: StreamReaderWriter
        :type initial_urls: list of str
        :type crawling_data: CrawlingData
        :type evaluator Evaluator
        :type workers_count: int
        """
        self._output_file = output_file
        self._workers_count = workers_count
        self._initial_urls = initial_urls
        self._crawling_data = crawling_data
        self._strategy_class = strategy_class
        self._evaluator = evaluator

    @LogWith(root_logger)
    def run(self):
        self._setup_server()
        self._setup_workers()
        self._start_workers()
        self._join_workers()
        self._log_workers_stats()
        pass

    def _setup_server(self):
        self._stats_listener = CrawlingStatsListener(self._output_file)
        self._stats_listener.write_headers()
        self._strategy = self._strategy_class(self._crawling_data, self._evaluator, self._stats_listener)
        self._strategy.feed_with_starting_urls(self._initial_urls)
        self._server = Server(self._strategy)

    def _setup_workers(self):
        self._workers = [
            Worker(uuid, self._server, PageRetriever(self._crawling_data))
            for uuid in xrange(self._workers_count)
        ]
        pass

    def _start_workers(self):
        for worker in self._workers:
            worker.start()
        pass

    def _join_workers(self):
        for worker in self._workers:
            worker.join()
        pass

    def _log_workers_stats(self):
        for worker in self._workers:
            root_logger.info(u'{} stats: {}'.format(worker.name, worker.get_stats()))


def _load_crawling_data(config):
    return ImportedCrawlingData.deserialize(config.crawling_data_file_path)


def evaluate_incoming_links(page, evaluator):
    for link in page.links_to:
        if evaluator.evaluate_link_score(link):
            return True
    else:
        return False


def get_matching_pages_count(crawling_data, evaluator):
    return len([page for page in crawling_data._pages.values() if evaluator.evaluate_page_score(page) or evaluate_incoming_links(page, evaluator)])


def get_all_pages_count(crawling_data):
    return len(crawling_data._pages)


def get_not_crawled_pages_count(crawling_data):
    return len([page for page in crawling_data._pages.values() if not page.is_crawled()])


def get_not_discovered_links_count(crawling_data):
    return len([page for page in crawling_data._pages.values() for link in page.links_from if
                link.is_not_discovered()])


def get_all_links_count(crawling_data):
    return sum(len(page.links_from) for page in crawling_data._pages.values())


def write_header(stats_file, strategy_class):
    stats_file.write(u'{}\n'.format(strategy_class.__name__))
    root_logger.info(u'-' * 120)
    root_logger.info(u'Using strategy {}'.format(strategy_class.__name__))
    root_logger.info(u'-' * 120)


def write_footer(crawling_data, stats_file, evaluator):
    write_log_to_stats_file_and_logger(u'Matching pages count: {:d}'.format(
        get_matching_pages_count(crawling_data, evaluator)),
        stats_file
    )
    write_log_to_stats_file_and_logger(u'Not discovered matching pages {}'.format(
        len(
            [page for page in crawling_data._pages.values()
             if page.is_not_discovered() and evaluator.evaluate_page_score(page)]
        )),
        stats_file
    )
    write_log_to_stats_file_and_logger(u'All pages count: {:d}'.format(get_all_pages_count(crawling_data)), stats_file)
    write_log_to_stats_file_and_logger(
        u'Not crawled pages count: {:d}'.format(get_not_crawled_pages_count(crawling_data)), stats_file)
    write_log_to_stats_file_and_logger(u'Not discovered links count: {:d}/{:d}'.format(
        get_not_discovered_links_count(crawling_data),
        get_all_links_count(crawling_data)),
        stats_file
    )


def write_log_to_stats_file_and_logger(msg, stats_file):
    root_logger.info(msg)
    stats_file.write(msg + u'\n')


@LogWith(root_logger)
def run_strategy(config, crawling_data, strategy_class, workers_count, evaluator, stats_file_name):
    crawling_data.reset()
    gc.collect()
    root_logger.info(u'Using stats file {}'.format(stats_file_name))
    with codecs.open(util.mkdirs(stats_file_name), 'w', 'utf-8') as stats_file:
        write_header(stats_file, strategy_class)
        Runner(workers_count, crawling_data, config.initial_urls, strategy_class, evaluator, stats_file).run()
        write_footer(crawling_data, stats_file, evaluator)


@LogWith(root_logger)
def main():
    config = Configurations.DEFAULT
    try:
        config.change_query(unicode(sys.argv[1].decode("utf-8")), int(sys.argv[2]))
    except IndexError:
        pass
    root_logger.info(u'Using Configuration {}'.format(config))

    evaluator = Evaluator(config.query)
    crawling_data = _load_crawling_data(config)
    for strategy_class in config.get_strategies():
        stats_file_name = config.stats_file_path_format.format(strategy_class.__name__)
        run_strategy(config, crawling_data, strategy_class, config.workers_count, evaluator, stats_file_name)
    pass


if __name__ == '__main__':
    main()