from heapq import heappush, heappop
import os
import itertools


def enum(**named):
    enums = dict(named)
    enums['reverse_mapping'] = dict((value, key) for key, value in enums.iteritems())
    return type('Enum', (), enums)


def mkdirs(file_path):
    dir_path = os.path.dirname(file_path)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    return file_path


class _PriorityQueue(object):
    def __init__(self, reverse_insertion_order=False):
        self._queue = []  # list of entries arranged in a heap
        self._entry_finder = {}  # mapping of entries to priorities
        self._REMOVED = '<removed-entry>'  # placeholder for a removed entry
        self._counter = itertools.count()  # unique sequence count
        self._removed_counter = 0  # removed pages counter for __len__
        self._reverse_insertion_order = reverse_insertion_order

    def put(self, priority, entry):
        """Add a new entry or update the priority of an existing object"""
        if entry.is_crawled():
            raise ValueError(u'Unexpected crawled page - {} {}'.format(entry._status, entry))
        if entry in self._entry_finder:
            self.__remove(entry)
        count = next(self._counter)
        if self._reverse_insertion_order:
            count *= -1
        mapped_entry = [priority, count, entry]
        self._entry_finder[entry] = mapped_entry
        heappush(self._queue, mapped_entry)

    def __remove(self, entry):
        """Mark an existing task as REMOVED.  Raise KeyError if not found."""
        mapped_entry = self._entry_finder.pop(entry)
        mapped_entry[-1] = self._REMOVED
        self._removed_counter += 1

    def pop(self):
        """Remove and return the lowest priority entry. Return None if empty."""
        while self._queue:
            priority, count, entry = heappop(self._queue)
            if entry is not self._REMOVED:
                del self._entry_finder[entry]
                return entry
            else:
                self._removed_counter -= 1
        return None

    def __len__(self):
        return len(self._queue) - self._removed_counter


class MaxPriorityQueue(_PriorityQueue):
    def put(self, priority, entry):
        super(MaxPriorityQueue, self).put(-priority, entry)
    pass


class MinPriorityQueue(_PriorityQueue):
    pass