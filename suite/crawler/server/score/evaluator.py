class Evaluator(object):
    def __init__(self, query):
        self._query = query
        self._max_score = 6.0
        self._0_score = 0.0
        self._body_score = 1 / self._max_score
        self._title_score = 2 / self._max_score
        self._link_score = 3 / self._max_score

    def _evaluate_page_body(self, page):
        return self._body_score if self._query in page.body_text else self._0_score

    def _evaluate_page_title(self, page):
        return self._title_score if self._query in page.title_text else self._0_score

    def evaluate_page_score(self, page):
        """
        :param page: suite.model.data.HtmlPage
        :return: float
        """
        links_score = self._link_score if list(
            [l for l in page.links_from if self.evaluate_link_score(l)]
        ) else self._0_score
        return self._evaluate_page_body(page) + self._evaluate_page_title(page) + links_score

    def _evaluate_link_text(self, link):
        return self._link_score if self._query in link.text else self._0_score

    def evaluate_link_score(self, link):
        """
        :param link: suite.model.data.Link
        :return: float
        """
        return self._evaluate_link_text(link)

    def evaluate_link_context_score(self, link):
        """
        :param link: suite.model.data.Link
        :return: float
        """
        return 1.0 if self._query in link.context else 0.0

    pass