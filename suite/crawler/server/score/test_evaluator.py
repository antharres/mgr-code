from unittest import TestCase

from suite.crawler.server.score.evaluator import Evaluator
from suite.model.data import HtmlPage, Link


class TestEvaluator(TestCase):
    def setUp(self):
        super(TestEvaluator, self).setUp()
        query = 'query'
        self._evaluator = Evaluator(query)
        self._max_score = 6.0

    def test_should_return_0_for_not_matching_page(self):
        page = HtmlPage(
            'url',
            'title',
            'body',
            [],
            []
        )

        self.assertEqual(0, self._evaluator.evaluate_page_score(page))
        pass

    def test_should_return_1_for_matching_page_body(self):
        page = HtmlPage(
            'url',
            'title',
            'body query test query',
            [],
            []
        )

        self.assertEqual(1 / self._max_score, self._evaluator.evaluate_page_score(page))
        pass

    def test_should_return_2_for_matching_page_title(self):
        page = HtmlPage(
            'url',
            'title query query',
            'body test',
            [],
            []
        )

        self.assertEqual(2 / self._max_score, self._evaluator.evaluate_page_score(page))
        pass

    def test_should_return_3_for_matching_page_title_and_body(self):
        page = HtmlPage(
            'url',
            'title query query',
            'body query test query',
            [],
            []
        )

        self.assertEqual(3 / self._max_score, self._evaluator.evaluate_page_score(page))
        pass

    def test_should_return_0_for_not_matching_link_text(self):
        link = Link(
            0,
            None,
            None,
            'test',
            'test',
            None,
        )

        self.assertEqual(0 / self._max_score, self._evaluator.evaluate_link_score(link))
        pass

    def test_should_return_3_for_matching_link_text(self):
        link = Link(
            0,
            None,
            None,
            'test',
            'test query query',
            None,
        )

        self.assertEqual(3 / self._max_score, self._evaluator.evaluate_link_score(link))
        pass

    def test_should_return_6_for_matching_page_title_body_and_links(self):
        links_from = []
        page = HtmlPage(
            'url',
            'title query query',
            'body query test query',
            links_from,
            []
        )

        links_from.extend([
            Link(
                0,
                page,
                None,
                'test',
                'test query query',
                None,
            ),
            Link(
                0,
                page,
                None,
                'test2',
                'test query query',
                None,
            )
        ])

        self.assertEqual(3 / self._max_score, self._evaluator.evaluate_page_score(page))
        pass

    def test_should_return_1_for_matching_link_context(self):
        link = Link(
            0,
            None,
            None,
            'test',
            'test query query',
            'query query',
        )

        self.assertEqual(1, self._evaluator.evaluate_link_context_score(link))
        pass

    pass