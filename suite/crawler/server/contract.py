from suite.model.data import HtmlPage


class ServerResponse(object):
    class ResponseType(object):
        WAITING = u'WAITING'
        FINISHED = u'FINISHED'
        URL = u'URL'

    def __init__(self, response_type, page_url=None):
        """
        :type response_type: Server.ServerResponse.ResponseType
        :type page_url: str
        """
        self.response_type = response_type
        self.page_url = page_url
        pass

    def __str__(self):
        return u'ServerResponse\n\t{}\n\t{}'.format(
            self.response_type,
            self.page_url
        )

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if not other:
            return False
        elif self.response_type != other.response_type:
            return False
        elif self.page_url != other.page_url:
            return False
        else:
            return True

    pass


class WorkerResponse(object):
    def __init__(self, worker_uuid, html_page, page_links):
        """
        :type page_links: list of str
        :type html_page: HtmlPage
        :type worker_uuid: int
        """
        self.worker_uuid = worker_uuid
        self.html_page = html_page
        self.page_links = page_links

    def __str__(self):
        return u'WorkerResponse\n\t{}\n\t{}\n\t{}'.format(
            self.worker_uuid,
            self.html_page,
            self.page_links
        )

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if not other:
            return False
        elif self.worker_uuid != other.worker_uuid:
            return False
        elif self.html_page != other.html_page:
            return False
        elif self.page_links != other.page_links:
            return False
        else:
            return True

    pass