from suite.crawler.server.strategy.strategy import Strategy
from suite.util import MaxPriorityQueue


class SharkSearchStrategy(Strategy):
    def __init__(self, crawling_data, evaluator, stats_listener):
        super(SharkSearchStrategy, self).__init__(crawling_data, evaluator, stats_listener)
        self._crawling_edge_priority_queue = MaxPriorityQueue(reverse_insertion_order=True)
        self._depth = 2
        self._decay = 0.5
        self._g = 0.7
        self._b = 0.8

    def _feed_with_starting_pages_internal(self, starting_pages):
        for page in starting_pages:
            page.set_depth(self._depth)
            self._crawling_edge_priority_queue.put(1, page)

        for page in self._crawling_data._pages.values():
            page.inherited_score = 1
        pass

    def _evaluate_neighborhood_score(self, link):
        anchor_score = self._evaluator.evaluate_link_score(link)
        if anchor_score > 0:
            anchor_context_score = 1.0
        else:
            anchor_context_score = self._evaluator.evaluate_link_context_score(link)
        return self._b * anchor_score + (1 - self._b) * anchor_context_score

    def update_rank_and_add_to_queue(self, links, new_depth, inherited_score):
        for (link, linked_page) in self._map_links_to_pages(
                self._filter_links(links, lambda l: not l.is_crawled())
        ):
            if linked_page.is_crawled():
                raise ValueError()

            neighborhood_score = self._evaluate_neighborhood_score(link)
            potential_rank = self._g * inherited_score + (1 - self._g) * neighborhood_score

            if linked_page.get_rank() < potential_rank:
                linked_page.set_rank(potential_rank)

            if linked_page.get_depth() < new_depth:
                linked_page.set_depth(new_depth)

            if new_depth > 0:
                self._crawling_edge_priority_queue.put(linked_page.get_rank(), linked_page)

    def _feed_links_internal(self, page, all_links, not_discovered_page_links, crawling_edge_page_links,
                             crawled_page_links):
        if page.get_score() == 0:
            new_depth = page.get_depth() - 1
            inherited_score = self._decay * page.get_score()
        else:
            new_depth = self._depth
            inherited_score = self._decay * page.inherited_score
        self.update_rank_and_add_to_queue(all_links, new_depth, inherited_score)

    def _get_next_page_from_crawling_edge(self):
        return self._crawling_edge_priority_queue.pop()

    def _get_crawling_edge_size(self):
        return len(self._crawling_edge_priority_queue)

    pass