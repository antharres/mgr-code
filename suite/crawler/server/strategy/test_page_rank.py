from logger import logger
from suite.crawler.server.strategy.page_rank import page_rank
from suite.model.data import Link, HtmlPage

root_logger = logger.get_logger()


def add_link(id, p_from, p_to):
    link = Link(id, p_from, p_to, None, None)
    p_from.links_from.append(link)
    p_to.links_to.append(link)
    return link


def add_page(pages, page):
    pages[page.url] = page
    page.mark_crawling_edge()

    return page


def set1():
    pages = {}

    pA = add_page(pages, HtmlPage('A', None, None, None, None))
    pB = add_page(pages, HtmlPage('B', None, None, None, None))
    pC = add_page(pages, HtmlPage('C', None, None, None, None))

    pD = add_page(pages, HtmlPage('D', None, None, None, None))
    pE = add_page(pages, HtmlPage('E', None, None, None, None))
    pF = add_page(pages, HtmlPage('F', None, None, None, None))

    # lAB = add_link(0, pA, pB)
    lBC = add_link(1, pB, pC)
    # lCA = add_link(2, pC, pA)
    #
    # lDE = add_link(3, pD, pE)
    # lDC = add_link(4, pD, pC)
    # lEF = add_link(4, pE, pF)
    #
    # lFA = add_link(4, pF, pA)

    return pages


# def set1():
#     pages = {}
#
#     pA = add_page(pages, HtmlPage('A', None, None, None, None))
#     pB = add_page(pages, HtmlPage('B', None, None, None, None))
#
#
#     lAB = add_link(0, pA, pB)
#
#     return pages


if __name__ == '__main__':
    x = set1()
    page_rank(x)
    for url, page in sorted(x._pages.items()):
        root_logger.info(u'\t{}: {}'.format(url, page.get_rank()))