import random

from suite.crawler.server.strategy.strategy import Strategy


class RandomStrategy(Strategy):
    def __init__(self, crawling_data, evaluator, stats_listener):
        super(RandomStrategy, self).__init__(crawling_data, evaluator, stats_listener)
        self._crawling_edge = []

    def _feed_with_starting_pages_internal(self, starting_pages):
        self._crawling_edge.extend(starting_pages)
        pass

    def _feed_links_internal(self, page, all_links, not_discovered_page_links, crawling_edge_page_links, crawled_page_links):
        self._crawling_edge.extend(
            map(
                lambda (link, linked_page): linked_page,
                self._map_links_to_pages(not_discovered_page_links)
            )
        )

    def _get_next_page_from_crawling_edge(self):
        crawling_edge_len = len(self._crawling_edge)
        if crawling_edge_len:
            index = random.randrange(0, crawling_edge_len)
            return self._crawling_edge.pop(index)
        else:
            return None

    def _get_crawling_edge_size(self):
        return len(self._crawling_edge)

