from suite.crawler.server.strategy.strategy import Strategy
from suite.util import MaxPriorityQueue


class FishSearchStrategy(Strategy):
    def __init__(self, crawling_data, evaluator, stats_listener):
        super(FishSearchStrategy, self).__init__(crawling_data, evaluator, stats_listener)
        self._crawling_edge_priority_queue = MaxPriorityQueue(reverse_insertion_order=True)
        self._width = 30
        self._depth = 2

    def _feed_with_starting_pages_internal(self, starting_pages):
        for page in starting_pages:
            page.set_depth(self._depth)
            self._crawling_edge_priority_queue.put(1, page)
        pass

    def update_rank_and_add_to_queue(self, links, matching_page_rank, new_depth):
        for (link, linked_page) in self._map_links_to_pages(
                self._filter_links(links, lambda l: not l.is_crawled())
        ):
            if linked_page.is_crawled():
                raise ValueError()
            potential_rank = matching_page_rank
            if linked_page.get_rank() < potential_rank:
                linked_page.set_rank(potential_rank)

            if linked_page.get_depth() < new_depth:
                linked_page.set_depth(new_depth)

            if new_depth > 0:
                self._crawling_edge_priority_queue.put(linked_page.get_rank(), linked_page)

    def _feed_links_internal(self, page, all_links, not_discovered_page_links, crawling_edge_page_links, crawled_page_links):
        if page.get_score() == 0:
            slice_point = self._width
            new_depth = page.get_depth() - 1
            left_part_rank = 0.5
            right_part_rank = 0
        else:
            new_depth = self._depth
            slice_point = int(self._width * 1.5)
            left_part_rank = 1.0
            right_part_rank = 0
        self.update_rank_and_add_to_queue(all_links[:slice_point], left_part_rank, new_depth)
        self.update_rank_and_add_to_queue(all_links[slice_point:], right_part_rank, new_depth)

    def _get_next_page_from_crawling_edge(self):
        return self._crawling_edge_priority_queue.pop()

    def _get_crawling_edge_size(self):
        return len(self._crawling_edge_priority_queue)

    pass