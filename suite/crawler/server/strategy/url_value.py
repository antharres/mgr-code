from suite.crawler.server.strategy.strategy import Strategy
from suite.util import MaxPriorityQueue


class UrlValueStrategy(Strategy):
    def __init__(self, crawling_data, evaluator, stats_listener):
        super(UrlValueStrategy, self).__init__(crawling_data, evaluator, stats_listener)
        self._crawling_edge_priority_queue = MaxPriorityQueue()

    def _feed_with_starting_pages_internal(self, starting_pages):
        for page in starting_pages:
            self._crawling_edge_priority_queue.put(1, page)
        pass

    def update_rank_and_add_to_queue(self, links):
        for (link, linked_page) in self._map_links_to_pages(links):
            link_score = self._evaluator.evaluate_link_score(link)
            if linked_page.get_rank() < link_score:
                linked_page.set_rank(link_score)
            self._crawling_edge_priority_queue.put(linked_page.get_rank(), linked_page)

    def _feed_links_internal(self, page, all_links, not_discovered_page_links, crawling_edge_page_links, crawled_page_links):
        self.update_rank_and_add_to_queue(not_discovered_page_links + crawling_edge_page_links)

    def _get_next_page_from_crawling_edge(self):
        return self._crawling_edge_priority_queue.pop()

    def _get_crawling_edge_size(self):
        return len(self._crawling_edge_priority_queue)

    pass