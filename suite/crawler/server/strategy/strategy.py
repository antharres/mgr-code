from logger import logger
from suite.crawler.server.contract import WorkerResponse
from suite.crawler.server.score.evaluator import Evaluator
from suite.model.data import CrawlingData, Link, HtmlPage


module_logger = logger.get_logger()


class Strategy(object):
    def __init__(self, crawling_data, evaluator, stats_listener):
        """
        :type crawling_data: CrawlingData
        :type evaluator Evaluator
        :type stats_listener: CrawlingStatsListener
        """
        self._evaluator = evaluator
        self._crawling_data = crawling_data
        self._stats_listener = stats_listener

    def feed_with_starting_urls(self, starting_urls):
        """
        :type starting_urls: set of str
        """
        starting_pages = []
        for url in starting_urls:
            page = self._crawling_data.get_page(url)
            starting_pages.append(page)
            page.mark_crawling_edge()

        self._feed_with_starting_pages_internal(starting_pages)
        pass

    def _feed_with_starting_pages_internal(self, starting_pages):
        """
        :type starting_pages: list of HtmlPage
        """
        raise NotImplementedError()

    def get_next_url(self):
        page = self._get_next_page_from_crawling_edge()

        while page:
            if page.is_crawled():
                # ignore - may happen if url1 is in progress by thread t1 and is
                # in links_from of another page processed by thread t2
                None
            elif page.is_crawling_edge():
                return page.url
            else:
                raise ValueError(u'Unexpected page outside crawling edge {} - {}'.format(page.url, page._status))
            page = self._get_next_page_from_crawling_edge()
        else:
            return None

    def _get_next_page_from_crawling_edge(self):
        """
        :rtype : HtmlPage
        """
        raise NotImplementedError()

    def _filter_links(self, page_links, page_status_function):
        return list(link for link in page_links if page_status_function(link.page_to))

    def _update_page_and_links_status(self, not_discovered_page_links, crawling_edge_page_links, crawled_page_links):
        for link in not_discovered_page_links:
            link.mark_crawling_edge()
            link.page_to.mark_crawling_edge()
        for link in crawling_edge_page_links:
            link.mark_crawling_edge()
        for link in crawled_page_links:
            link.mark_crawled_skip()

    def feed_with_links(self, worker_response):
        """
        :type worker_response: WorkerResponse
        """
        html_page = worker_response.html_page
        page_url = html_page.url
        if html_page.is_not_discovered():
            raise ValueError(u'Unexpected not discovered page {}'.format(page_url))
        elif html_page.is_crawling_edge():
            module_logger.trace(u'\t\tProcessing crawled link {}'.format(page_url))
            html_page.mark_crawled()

            not_discovered_page_links = self._filter_links(worker_response.page_links, lambda x: x.is_not_discovered())
            crawling_edge_page_links = self._filter_links(worker_response.page_links, lambda x: x.is_crawling_edge())
            crawled_page_links = self._filter_links(worker_response.page_links, lambda x: x.is_crawled())

            page_score = self._evaluator.evaluate_page_score(html_page)
            html_page.set_score(page_score)

            self._feed_links_internal(
                html_page,
                worker_response.page_links,
                not_discovered_page_links,
                crawling_edge_page_links,
                crawled_page_links
            )

            self._update_page_and_links_status(
                not_discovered_page_links,
                crawling_edge_page_links,
                crawled_page_links
            )

            crawling_edge_size = self._get_crawling_edge_size()
            self._stats_listener.notify_work_finished(worker_response, crawling_edge_size)
        else:
            module_logger.trace(u'\t\tIgnoring previously visited link {}'.format(page_url))
        pass

    def _feed_links_internal(self, page, all_links, not_discovered_page_links, crawling_edge_page_links, crawled_page_links):
        """
        :type page: HtmlPage
        :type all_links: list of Link
        :type not_discovered_page_links: list of Link
        :type crawling_edge_page_links: list of Link
        :type crawled_page_links: list of Link
        """
        raise NotImplementedError()

    def _get_crawling_edge_size(self):
        raise NotImplementedError()

    def _map_links_to_pages(self, links):
        """
        :type links: list of Link
        :rtype: list of tuple(Link, HtmlPage)
        """
        return map(lambda link: (link, link.page_to), links)


