from logger import logger
from suite.crawler.server.strategy.page_rank import page_rank
from suite.crawler.server.strategy.strategy import Strategy
from suite.util import MaxPriorityQueue

root_logger = logger.get_logger()


class SharkRankStrategy(Strategy):
    def __init__(self, crawling_data, evaluator, stats_listener):
        super(SharkRankStrategy, self).__init__(crawling_data, evaluator, stats_listener)
        self._reordering_period_threshold = 1000
        self._crawled_page_counter = 0
        self._crawling_edge_priority_queue = MaxPriorityQueue(reverse_insertion_order=True)
        self._depth = 2
        self._decay = 0.8
        self._g = 0.7
        self._b = 0.8

        self._w = 0.7

    def _feed_with_starting_pages_internal(self, starting_pages):
        for page in starting_pages:
            page.set_depth(self._depth)
            self._crawling_edge_priority_queue.put(1.0, page)

        for page in self._crawling_data._pages.values():
            page.inherited_score = 1.0
            page.shark_rank = 0.0
        pass

    def _extract_queued_pages(self):
        current_pages = []
        while True:
            next_page = self._crawling_edge_priority_queue.pop()
            if not next_page:
                break
            current_pages.append(next_page)
        return current_pages

    def get_combined_rank(self, page):
        return (1.0 - self._w) * page.get_rank() + self._w * page.shark_rank

    def _reorganize_queue(self):
        page_rank(self._crawling_data._pages)
        for page in self._extract_queued_pages():
            self._crawling_edge_priority_queue.put(self.get_combined_rank(page), page)

    def _evaluate_neighborhood_score(self, link):
        anchor_score = self._evaluator.evaluate_link_score(link)
        if anchor_score > 0:
            anchor_context_score = 1.0
        else:
            anchor_context_score = self._evaluator.evaluate_link_context_score(link)
        return self._b * anchor_score + (1 - self._b) * anchor_context_score

    def update_rank_and_add_to_queue(self, links, new_depth, inherited_score):
        for (link, linked_page) in self._map_links_to_pages(
                self._filter_links(links, lambda l: not l.is_crawled())
        ):
            if linked_page.is_crawled():
                raise ValueError()

            neighborhood_score = self._evaluate_neighborhood_score(link)
            potential_rank = self._g * inherited_score + (1 - self._g) * neighborhood_score

            if linked_page.shark_rank < potential_rank:
                linked_page.shark_rank = potential_rank

            if linked_page.get_depth() < new_depth:
                linked_page.set_depth(new_depth)

            if new_depth > 0:
                self._crawling_edge_priority_queue.put(linked_page.shark_rank, linked_page)

    def _feed_links_internal(self, page, all_links, not_discovered_page_links, crawling_edge_page_links,
                             crawled_page_links):
        self._crawled_page_counter += 1
        if page.get_score() == 0:
            new_depth = page.get_depth() - 1
            inherited_score = self._decay * page.get_score()
        else:
            new_depth = self._depth
            inherited_score = self._decay * page.inherited_score
        self.update_rank_and_add_to_queue(all_links, new_depth, inherited_score)
        if self._crawled_page_counter % self._reordering_period_threshold == 0:
            root_logger.info(u'Crawled {} pages - reorganizing crawling edge queue'.format(self._crawled_page_counter))
            self._reorganize_queue()

    def _get_next_page_from_crawling_edge(self):
        return self._crawling_edge_priority_queue.pop()

    def _get_crawling_edge_size(self):
        return len(self._crawling_edge_priority_queue)

    pass