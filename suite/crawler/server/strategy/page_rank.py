from logger import logger
from logger.logger import LogWith
from suite.crawler.server.strategy.strategy import Strategy
from suite.util import MaxPriorityQueue


root_logger = logger.get_logger()


class PageRankStrategy(Strategy):
    def __init__(self, crawling_data, evaluator, stats_listener):
        super(PageRankStrategy, self).__init__(crawling_data, evaluator, stats_listener)
        self._crawling_edge_priority_queue = MaxPriorityQueue()
        self._crawled_page_counter = 0
        self._reordering_period_threshold = 1000

    def _feed_with_starting_pages_internal(self, starting_pages):
        page_rank(self._crawling_data._pages)
        for page in starting_pages:
            self._crawling_edge_priority_queue.put(page.get_rank(), page)
        pass

    def _extract_queued_pages(self):
        current_pages = []
        while True:
            next_page = self._crawling_edge_priority_queue.pop()
            if not next_page:
                break
            current_pages.append(next_page)
        return current_pages

    def _reorganize_queue(self):
        page_rank(self._crawling_data._pages)
        for page in self._extract_queued_pages():
            self._crawling_edge_priority_queue.put(page.get_rank(), page)

    def _feed_links_internal(self, page, all_links, not_discovered_page_links, crawling_edge_page_links, crawled_page_links):
        self._crawled_page_counter += 1
        for l, page in self._map_links_to_pages(not_discovered_page_links):
            self._crawling_edge_priority_queue.put(0.0, page)
        if self._crawled_page_counter % self._reordering_period_threshold == 0:
            root_logger.info(u'Crawled {} pages - reorganizing crawling edge queue'.format(self._crawled_page_counter))
            self._reorganize_queue()
        pass

    def _get_next_page_from_crawling_edge(self):
        return self._crawling_edge_priority_queue.pop()

    def _get_crawling_edge_size(self):
        return len(self._crawling_edge_priority_queue)

    pass


@LogWith(root_logger)
def page_rank(pages, d=0.85, eps=.0001, enable_logging=False):
    """
    :type pages: dict of HtmlPage
    """
    all_discovered_pages = filter(lambda p: not p.is_not_discovered(), pages.values())
    n = len(all_discovered_pages)
    # init
    for initial_page in all_discovered_pages:
        initial_page.old_rank = 1.0 / n
        pass

    diff = 100
    i = 0
    while diff > eps:
        if enable_logging:
            root_logger.info(u'PageRank starting iteration {} - diff {}, eps {}'.format(i, diff, eps))
        # dangling nodes - teleport probability
        dp = 0.0
        dangling_pages = filter(lambda p: not p.links_from, all_discovered_pages)
        for dangling_page in dangling_pages:
            dp += d * dangling_page.old_rank / n
            pass

        new_rank_sum = 0.0
        for all_page in all_discovered_pages:
            new_rank = 1.0/ n # TODO old
            new_rank = all_page.get_score() / n # TODO new - focused
            all_page.set_rank(new_rank)
            new_rank_sum += new_rank
        for all_page in all_discovered_pages:
            new_rank = all_page.get_rank() / new_rank_sum if new_rank_sum else all_page.get_rank()
            new_rank = dp + (1.0 - d) * new_rank
            all_inbound_pages = all_page.get_all_discovered_inbound_pages()
            for l, inbound_link_page in all_inbound_pages:
                new_rank += d * inbound_link_page.old_rank / len(inbound_link_page.links_from)
                pass
            all_page.set_rank(new_rank)
            # root_logger.info(u'\t{}: {}'.format(all_page.url, all_page.get_rank()))
        diff = 0.0

        for page in all_discovered_pages:
            new_rank = page.get_rank()
            diff += abs(page.old_rank - new_rank)
            page.old_rank = new_rank
            pass
        if enable_logging:
            root_logger.info(u'PageRank ending iteration {} - diff {}, eps {}'.format(i, diff, eps))
        i += 1
