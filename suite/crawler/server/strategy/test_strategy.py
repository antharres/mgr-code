from unittest import TestCase

from mock import Mock, MagicMock, call

from suite.crawler.server.contract import WorkerResponse
from suite.crawler.server.score.evaluator import Evaluator
from suite.crawler.server.strategy.bfs import BfsStrategy
from suite.crawler.server.strategy.dfs import DfsStrategy
from suite.crawler.server.strategy.fish_search import FishSearchStrategy
from suite.crawler.server.strategy.page_rank import PageRankStrategy
from suite.crawler.server.strategy.page_url_value import PageUrlValueStrategy
from suite.crawler.server.strategy.page_value import PageValueStrategy
from suite.crawler.server.strategy.rndm import RandomStrategy
from suite.crawler.server.strategy.shark_search import SharkSearchStrategy
from suite.crawler.server.strategy.strategy import Strategy
from suite.crawler.server.strategy.url_value import UrlValueStrategy
from suite.crawler.stats.listeners import CrawlingStatsListener
from suite.model.data import CrawlingData, HtmlPage

CRAWLING_EDGE_SIZE = 15


class TestStrategy(TestCase):
    def setUp(self):
        self._worker_uuid = 0
        self._mock_stats_listener = Mock(name='MockCrawlingStatsListener', spec=CrawlingStatsListener)
        self._mock_crawling_data = Mock(name='MockCrawlingStatsListener', spec=CrawlingData)
        self._mock_evaluator = Mock(name='Evaluator', spec=Evaluator)
        self._strategy = Strategy(self._mock_crawling_data, self._mock_evaluator, self._mock_stats_listener)
        self._strategy._get_crawling_edge_size = lambda: CRAWLING_EDGE_SIZE
        self._strategy._feed_links_internal = lambda page, all_links, unknown_page_links, crawling_edge_page_links, crawled_page_links: None

    # --------------------------------------------------------------
    # stats listener
    # --------------------------------------------------------------
    def test_should_notify_stats_listener(self):
        page_url = 'page_url'
        mock_page = Mock(name='MockHtmlPage', spec=HtmlPage)
        mock_page.url = page_url
        mock_page.is_not_discovered.return_value = False
        mock_page.is_crawling_edge.return_value = True

        response = WorkerResponse(self._worker_uuid, mock_page, [])
        self._strategy.feed_with_links(response)

        self._mock_stats_listener.notify_work_finished.assert_has_calls(
            call(response, CRAWLING_EDGE_SIZE)
        )

        pass

    def test_should_not_notify_stats_listener_twice_with_the_same_page(self):
        page_url = 'page_url'
        mock_page = MagicMock(name='MockHtmlPage', spec=HtmlPage)
        mock_page.url = page_url
        mock_page.is_not_discovered.return_value = False
        mock_page.is_crawling_edge.return_value = True

        response = WorkerResponse(self._worker_uuid, mock_page, [])

        self._strategy.feed_with_links(response)
        self._strategy.feed_with_links(response)

        self._mock_stats_listener.notify_work_finished.assert_has_calls([
            call(response, CRAWLING_EDGE_SIZE)
        ])

    def test_create_strategies(self):
        for strategy_class in [
            BfsStrategy,
            DfsStrategy,
            RandomStrategy,
            PageValueStrategy,
            UrlValueStrategy,
            PageUrlValueStrategy,
            FishSearchStrategy,
            SharkSearchStrategy,
            PageRankStrategy,
        ]:
            strategy_class(self._mock_crawling_data, self._mock_evaluator, self._mock_stats_listener)

        pass