import itertools

from suite.crawler.server.strategy.strategy import Strategy
from suite.util import MinPriorityQueue


class BfsStrategy(Strategy):
    def __init__(self, crawling_data, evaluator, stats_listener):
        super(BfsStrategy, self).__init__(crawling_data, evaluator, stats_listener)
        self._crawling_edge_priority_queue = MinPriorityQueue()
        self._counter = itertools.count()

    def _feed_with_starting_pages_internal(self, starting_pages):
        for page in starting_pages:
            self._queue_page(page)
        pass

    def _queue_page(self, p):
        self._crawling_edge_priority_queue.put(next(self._counter), p)

    def _feed_links_internal(self, page, all_links, not_discovered_page_links, crawling_edge_page_links, crawled_page_links):
        for (link, linked_page) in self._map_links_to_pages(not_discovered_page_links):
            self._queue_page(linked_page)

    def _get_next_page_from_crawling_edge(self):
        return self._crawling_edge_priority_queue.pop()

    def _get_crawling_edge_size(self):
        return len(self._crawling_edge_priority_queue)

    pass

