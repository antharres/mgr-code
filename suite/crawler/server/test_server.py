from unittest import TestCase

from mock import Mock

from suite.crawler.server.contract import ServerResponse, WorkerResponse
from suite.crawler.server.server import Server
from suite.crawler.server.strategy.strategy import Strategy
from suite.model.data import HtmlPage


class TestServer(TestCase):
    def setUp(self):
        self._mock_strategy = Mock(name='Strategy', spec=Strategy)
        self._mock_strategy.get_next_url.return_value = None

        self._server = Server(self._mock_strategy)

        self._worker_uuid = 0

    def _setup_strategy_response(self, response):
        """
        :type response: list of str
        """
        self._mock_strategy.get_next_url.side_effect = response
        pass

    def test_should_throw_an_exception_when_work_finished_by_unknown_worker(self):
        mock_page = Mock(name='MockHtmlPage', spec=HtmlPage)
        mock_page.url = 'test_url'

        self.assertRaises(ValueError, self._server.work_finished,
                          (WorkerResponse(self._worker_uuid, mock_page, [])))
        pass

    def test_should_accept_work_finished_from_known_worker(self):
        page_url = 'page_url'
        self._setup_strategy_response([
            page_url
        ])
        mock_page = Mock(name='MockHtmlPage', spec=HtmlPage)
        mock_page.url = page_url

        server_response = self._server.get_next_url(self._worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.URL, page_url),
            server_response
        )

        self._server.work_finished(WorkerResponse(self._worker_uuid, mock_page, []))
        pass

    def test_should_raise_value_error_when_asked_for_links_by_worker_with_work_in_progress(self):
        page_url = 'page_url'
        self._setup_strategy_response([
            page_url
        ])
        server_response = self._server.get_next_url(self._worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.URL, page_url),
            server_response
        )

        self.assertRaises(ValueError, self._server.get_next_url, self._worker_uuid)
        pass

    def test_should_not_raise_value_error_when_asked_for_links_by_different_workers(self):
        worker_uuid2 = 1

        page_url_1, page_url_2 = 'page_url_1', 'page_url_2'

        self._setup_strategy_response([
            page_url_1,
            page_url_2
        ])

        server_response = self._server.get_next_url(self._worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.URL, page_url_1),
            server_response
        )

        server_response = self._server.get_next_url(worker_uuid2)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.URL, page_url_2),
            server_response
        )
        pass

    def test_should_accept_work_finished_from_known_worker_after_receiving_the_previous_response(self):
        page_url = 'page_url'
        self._setup_strategy_response([
            page_url,
            None
        ])
        mock_page = Mock(name='MockHtmlPage', spec=HtmlPage)
        mock_page.url = page_url

        server_response = self._server.get_next_url(self._worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.URL, page_url),
            server_response
        )

        self._server.work_finished(WorkerResponse(self._worker_uuid, mock_page, []))
        server_response = self._server.get_next_url(self._worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.FINISHED),
            server_response
        )
        pass

    # --------------------------------------------------------------
    # strategy interaction
    # --------------------------------------------------------------
    def test_should_return_url_returned_by_strategy(self):
        page_url = 'page_url'
        self._setup_strategy_response([
            page_url
        ])

        server_response = self._server.get_next_url(self._worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.URL, page_url),
            server_response
        )
        pass

    def test_should_return_waiting_when_strategy_returns_None_and_waiting_for_response(self):
        page_url = 'page_url'
        self._setup_strategy_response([
            page_url,
            None
        ])
        self._server.get_next_url(1)
        server_response = self._server.get_next_url(self._worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.WAITING),
            server_response
        )
        pass

    def test_should_return_finished_when_strategy_returns_None(self):
        server_response = self._server.get_next_url(self._worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.FINISHED),
            server_response
        )
        pass

    def test_should_allow_subsequent_calls_from_worker_when_waiting_for_result(self):
        another_worker_uuid = 1
        page_url = 'page_url'
        self._setup_strategy_response([
            page_url,
            None,
            None,
            None,
            None
        ])
        mock_page = Mock(name='MockHtmlPage', spec=HtmlPage)
        mock_page.url = page_url

        server_response = self._server.get_next_url(another_worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.URL, page_url),
            server_response
        )

        server_response = self._server.get_next_url(self._worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.WAITING),
            server_response
        )

        server_response = self._server.get_next_url(self._worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.WAITING),
            server_response
        )

        self._server.work_finished(WorkerResponse(another_worker_uuid, mock_page, []))

        server_response = self._server.get_next_url(self._worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.FINISHED),
            server_response
        )

        server_response = self._server.get_next_url(another_worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.FINISHED),
            server_response
        )
        pass

    def test_should_throw_value_error_when_called_work_finished_with_different_url_than_expected(self):
        page_url = 'page_url'
        self._setup_strategy_response([
            page_url
        ])
        server_response = self._server.get_next_url(self._worker_uuid)
        mock_page = Mock(name='MockHtmlPage', spec=HtmlPage)
        mock_page.url = 'different_page'

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.URL, page_url),
            server_response
        )

        self.assertRaises(ValueError, self._server.work_finished,
                          WorkerResponse(self._worker_uuid, mock_page, []))
        pass

    def test_should_pass_worker_response_to_strategy(self):
        page_url = 'page_url'
        self._setup_strategy_response([
            page_url
        ])
        mock_page = Mock(name='MockHtmlPage', spec=HtmlPage)
        mock_page.url = page_url

        server_response = self._server.get_next_url(self._worker_uuid)

        self.assertEqual(
            ServerResponse(ServerResponse.ResponseType.URL, page_url),
            server_response
        )

        response = WorkerResponse(self._worker_uuid, mock_page, [])
        self._server.work_finished(response)

        self._mock_strategy.feed_with_links.assert_called_with(
            response
        )

        pass

    pass