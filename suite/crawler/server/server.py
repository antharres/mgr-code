from threading import Lock

from suite.crawler.server.contract import ServerResponse
from suite.crawler.server.strategy.strategy import Strategy


class Server(object):
    def __init__(self, strategy):
        """
        :type strategy: Strategy
        """
        self._lock = Lock()
        self._work_in_progress = {}
        self._strategy = strategy
        self._initial_urls = []

    def _get_next_url_internal(self, worker_uuid):
        next_url = self._strategy.get_next_url()

        if next_url is not None:
            self._work_in_progress[worker_uuid] = next_url
            return ServerResponse(ServerResponse.ResponseType.URL, next_url)
        elif self._work_in_progress:
            return ServerResponse(ServerResponse.ResponseType.WAITING)
        else:
            return ServerResponse(ServerResponse.ResponseType.FINISHED)

    def get_next_url(self, worker_uuid):
        """
        :rtype : Server.ServerResponse
        """
        with self._lock:
            if self._work_in_progress.get(worker_uuid, None):
                raise ValueError(
                    u'Worker {} is expected to finish before calling for next urls again'.format(worker_uuid))

            return self._get_next_url_internal(worker_uuid)

        pass

    def work_finished(self, worker_response):
        """
        :type worker_response suite.crawler.server.contract.WorkerResponse
        """
        with self._lock:
            page_to_process_by_worker = self._work_in_progress.get(worker_response.worker_uuid, None)

            if page_to_process_by_worker != worker_response.html_page.url:
                raise ValueError(u'Unexpected response from worker {}. Expected {}, but got {}'.format(
                    worker_response.worker_uuid,
                    page_to_process_by_worker,
                    worker_response.html_page.url)
                )
            else:
                del self._work_in_progress[worker_response.worker_uuid]
                self._strategy.feed_with_links(worker_response)
            pass
        pass

    pass