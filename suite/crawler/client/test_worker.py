from unittest import TestCase

from mock import Mock, call, MagicMock

from suite.crawler.client.retriever import PageRetriever
from suite.crawler.server.contract import ServerResponse, WorkerResponse
from suite.crawler.client.worker import Worker, UnsupportedServerResponseTypeException, PageNotFoundException
from suite.crawler.server.server import Server


class TestWorker(TestCase):
    def setUp(self):
        self._worker_uuid = 0
        self._mock_server = Mock(name='Server', spec=Server)
        self._mock_retriever = MagicMock(name='Retriever', spec=PageRetriever)

        self._worker = Worker(self._worker_uuid, self._mock_server, self._mock_retriever, timeout=0.0)
        pass

    def test_should_wait_when_server_returns_running(self):
        self._mock_server.get_next_url.side_effect = [
            ServerResponse(ServerResponse.ResponseType.WAITING),
            ServerResponse(ServerResponse.ResponseType.WAITING),
            ServerResponse(ServerResponse.ResponseType.FINISHED)
        ]

        self._worker.run()

        self._mock_server.get_next_url.assert_has_calls([
            call(self._worker_uuid),
            call(self._worker_uuid),
            call(self._worker_uuid)
        ])
        pass

    def test_should_return_links_to_server(self):
        page_url = 'page_url'
        mock_page = Mock(name='HtmlPage')

        mock_page_links = []
        mock_page.links_from = mock_page_links

        def retrieve_page_with_links(*args):
            return mock_page

        self._mock_retriever.retrieve_page_with_links.side_effect = retrieve_page_with_links

        self._mock_server.get_next_url.side_effect = [
            ServerResponse(ServerResponse.ResponseType.URL, page_url),
            ServerResponse(ServerResponse.ResponseType.FINISHED)
        ]

        self._worker.run()

        self._mock_server.get_next_url.assert_has_calls([
            call(self._worker_uuid),
            call(self._worker_uuid)
        ])
        self._mock_server.work_finished.assert_called_with(
            WorkerResponse(self._worker_uuid, mock_page, mock_page_links)
        )
        pass

    def test_should_raise_unsupported_server_response_type_exception_when_server_returns_unexpected_response_type(self):
        self._mock_server.get_next_url.side_effect = [
            ServerResponse(u'INVALID')
        ]

        self.assertRaises(UnsupportedServerResponseTypeException, self._worker.run)

        self._mock_server.get_next_url.assert_has_calls([
            call(self._worker_uuid)
        ])
        pass

    def test_should_raise_page_not_found_exception_when_page_does_not_exist(self):
        self._mock_server.get_next_url.side_effect = [
            ServerResponse(ServerResponse.ResponseType.URL, u'not-existing-url')
        ]

        self._mock_retriever.retrieve_page_with_links.side_effect = [
            KeyError
        ]

        self.assertRaises(PageNotFoundException, self._worker.run)

        self._mock_server.get_next_url.assert_has_calls([
            call(self._worker_uuid)
        ])
        pass