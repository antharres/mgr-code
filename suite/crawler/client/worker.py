from threading import Thread
import time

from logger import logger
from suite.crawler.server.contract import ServerResponse, WorkerResponse
from suite.crawler.client.retriever import PageRetriever
from suite.model.data import HtmlPage


module_logger = logger.get_logger()


class WorkerException(Exception):
    pass


class UnsupportedServerResponseTypeException(WorkerException):
    pass


class PageNotFoundException(WorkerException):
    pass


class Worker(Thread):
    def __init__(self, uuid, server, url_retriever, timeout=0.5):
        """
        :type uuid: int
        :type server: Server
        :type timeout: float
        :type url_retriever: PageRetriever
        """
        super(Worker, self).__init__(name=u'Worker_{}'.format(uuid))
        self._uuid = uuid
        self._server = server
        self._timeout = timeout
        self._url_retriever = url_retriever

        self._pages_processed = 0
        self._waiting_count = 0

    def _log_info(self, message):
        module_logger.info(u'Worker_{} - {}'.format(self._uuid, message))
        pass

    def _log_warn(self, message, exception=None):
        module_logger.warn(u'Worker_{} - {}'.format(self._uuid, message))
        if exception:
            module_logger.exception(exception)
        pass

    def _handle_server_finished(self):
        self._log_info(u'Finishing work'.format(self._uuid))
        return

    def _handle_server_waiting(self):
        self._waiting_count += 1
        self._log_info(u'Sleeping')
        time.sleep(self._timeout)
        pass

    def _retrieve_page(self, url):
        """
        :rtype : HtmlPage
        """
        return self._url_retriever.retrieve_page_with_links(url)

    def _handle_server_url(self, response):
        self._log_info(u'Crawling page {}'.format(response.page_url))
        try:
            page = self._retrieve_page(response.page_url)
            if page:
                self._pages_processed += 1
                self._server.work_finished(
                    WorkerResponse(self._uuid, page, page.links_from))
        except KeyError:
            raise PageNotFoundException(u'Can\'t find page {}'.format(response.page_url))

        pass

    def run(self):
        response = object()
        while response is not None:
            response = self._server.get_next_url(self._uuid)

            response_type = response.response_type
            # self._log_info(u'Received response from server: {}'.format(response_type))
            if response_type == ServerResponse.ResponseType.FINISHED:
                return self._handle_server_finished()
            elif response_type == ServerResponse.ResponseType.WAITING:
                self._handle_server_waiting()
            elif response_type == ServerResponse.ResponseType.URL:
                self._handle_server_url(response)
            else:
                raise UnsupportedServerResponseTypeException(u'Unexpected response_type: {}'.format(response_type))
        pass

    def get_stats(self):
        """
        :rtype (int, int)
        """
        return {
            u'Pages processed': self._pages_processed,
            u'Waiting count': self._waiting_count
        }

