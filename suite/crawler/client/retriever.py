from suite.model.data import CrawlingData


class PageRetriever(object):
    def __init__(self, crawling_data):
        """
        :type crawling_data: CrawlingData
        """
        self._crawling_data = crawling_data

    def retrieve_page_with_links(self, url):
        """
        :type url: str
        :rtype HtmlPage
        """
        return self._crawling_data.get_page(url)

