from suite.crawler.server.contract import WorkerResponse


class CrawlingStatsListener(object):
    _DEFAULT_SCORE_THRESHOLD = 0.0

    def __init__(self, output_file, score_threshold=_DEFAULT_SCORE_THRESHOLD):
        """
        :type score_threshold: float
        :type output_file: StreamReaderWriter
        """
        self._score_threshold = score_threshold
        self._matching_page_count = 0
        self._page_count = 0
        self._output_file = output_file

    def write_headers(self):
        line = u'{:s}:\t{:s}\t{:s}\t{:s}\t{:s}\n'.format(
            u'Page count',
            u'Matching page count',
            u'Crawling edge size',
            u'Page score',
            u'Page url'
        )
        self._output_file.write(line)

    def notify_work_finished(self, worker_response, crawling_edge_size):
        """
        :type worker_response WorkerResponse
        """
        self._page_count += 1
        if worker_response.html_page.get_score() > CrawlingStatsListener._DEFAULT_SCORE_THRESHOLD:
            self._matching_page_count += 1

        line = u'{:d}:\t{:d}\t{:d}\t{:f}\t{:s}\n'.format(
            self._page_count,
            self._matching_page_count,
            crawling_edge_size,
            worker_response.html_page.get_score(),
            worker_response.html_page.url
        )
        self._output_file.write(line)