from unittest import TestCase

from suite.model.data import HtmlPage
from suite.util import MaxPriorityQueue, MinPriorityQueue


class TestMaxPriorityQueue(TestCase):

    def setUp(self):
        self._queue = MaxPriorityQueue()

    def test_should_return_empty_value_when_queue_is_empty(self):
        self.assertIsNone(self._queue.pop())
        self.assertEqual(0, len(self._queue))

    def test_should_return_maximum_value(self):
        v1 = self._html_page('test1')
        self._queue.put(0, v1)
        v2 = self._html_page('test2')
        self._queue.put(1, v2)

        self.assertEqual(v2, self._queue.pop())
        self.assertEqual(v1, self._queue.pop())
        self.assertEqual(0, len(self._queue))
        self.assertIsNone(self._queue.pop())

    def _html_page(self, url):
        v1 = HtmlPage(url, None, None, None, None)
        v1.mark_crawling_edge()
        return v1

    def test_should_return_maximum_value_after_priority_update(self):
        v1 = self._html_page('test1')
        self._queue.put(0, v1)
        v2 = self._html_page('test2')
        self._queue.put(1, v2)

        self._queue.put(1000, v1)
        self.assertEqual(2, len(self._queue))

        self.assertEqual(v1, self._queue.pop())
        self.assertEqual(v2, self._queue.pop())
        self.assertEqual(0, len(self._queue))
        self.assertIsNone(self._queue.pop())

    pass


class TestMinPriorityQueue(TestCase):

    def setUp(self):
        self._queue = MinPriorityQueue()

    def test_should_return_empty_value_when_queue_is_empty(self):
        self.assertIsNone(self._queue.pop())
        self.assertEqual(0, len(self._queue))

    def test_should_return_maximum_value(self):
        v1 = self._html_page('test1')
        self._queue.put(1, v1)
        v2 = self._html_page('test2')
        self._queue.put(0, v2)

        self.assertEqual(v2, self._queue.pop())
        self.assertEqual(v1, self._queue.pop())
        self.assertEqual(0, len(self._queue))
        self.assertIsNone(self._queue.pop())

    def _html_page(self, url):
        v1 = HtmlPage(url, None, None, None, None)
        v1.mark_crawling_edge()
        return v1

    def test_should_return_maximum_value_after_priority_update(self):
        v1 = self._html_page('test1')
        self._queue.put(100, v1)
        v2 = self._html_page('test2')
        self._queue.put(50, v2)

        self._queue.put(1, v1)
        self.assertEqual(2, len(self._queue))

        self.assertEqual(v1, self._queue.pop())
        self.assertEqual(v2, self._queue.pop())
        self.assertEqual(0, len(self._queue))
        self.assertIsNone(self._queue.pop())

    pass
