# coding=utf-8
from suite.model.stop_words import polish, english
from suite.util import enum


class Configuration(object):
    def get_strategies(self):
        from suite.crawler.server.strategy.bfs import BfsStrategy
        from suite.crawler.server.strategy.dfs import DfsStrategy
        from suite.crawler.server.strategy.fish_search import FishSearchStrategy
        from suite.crawler.server.strategy.page_url_value import PageUrlValueStrategy
        from suite.crawler.server.strategy.page_value import PageValueStrategy
        from suite.crawler.server.strategy.rndm import RandomStrategy
        from suite.crawler.server.strategy.shark_search import SharkSearchStrategy
        from suite.crawler.server.strategy.url_value import UrlValueStrategy
        from suite.crawler.server.strategy.page_rank import PageRankStrategy
        from suite.crawler.server.strategy.shark_rank import SharkRankStrategy

        return [
            BfsStrategy,
            DfsStrategy,
            RandomStrategy,
            PageValueStrategy,
            UrlValueStrategy,
            PageUrlValueStrategy,
            FishSearchStrategy,
            SharkSearchStrategy,
            PageRankStrategy,
            SharkRankStrategy,
        ]

    def __init__(self, name, data_root_dir, include_external_links,
                 initial_urls, query, stop_words, encoding=u'iso8859_2'):
        self.stop_words = stop_words
        self.name = name
        self.data_root_dir = data_root_dir
        self.include_external_links = include_external_links
        self.initial_urls = initial_urls
        self.encoding = encoding

        self.link_context_radius = 5
        self.output_dir_path = u'output/{}/'.format(self.name)
        self.gephx_file_path = self.output_dir_path + u'gephi/crawling_data.gexf'
        self.crawling_data_file_path = self.output_dir_path + u'crawling_data.dat'

        self.change_query(query, 1)

    def change_query(self, query, workers_count):
        self.workers_count = workers_count
        self.query = query
        self.stats_dir_path = self.output_dir_path + u'stats/{}_{}/'.format(self.query, self.workers_count)
        self.stats_file_path_format = self.stats_dir_path + u'crawling_stats_{}.txt'

    def __str__(self):
        return u'Configuration: {}' \
               u'\n\tdata: {}' \
               u'\n\tcrawling_data_file_path: {}' \
               u'\n\tinclude_external_links: {}' \
               u'\n\tinitial_urls: {}' \
               u'\n\tquery: {}' \
               u'\n\tworkers_count: {}' \
               u'\n\tstrategies: {}' \
               u'\n\tencoding: {}' \
            .format(
            self.name,
            self.data_root_dir,
            self.crawling_data_file_path,
            self.include_external_links,
            self.initial_urls,
            self.query,
            self.workers_count,
            u'[{}]'.format(u', '.join(s.__name__ for s in self.get_strategies())),
            self.encoding
        )


Configurations = enum(
    WIKI_SCHOOLS=Configuration(
        u'WIKI_SCHOOLS',
        # u'/media/antharres/Kingston-SSD/mgr/wiki/wiki/',
        u'/media/antharres/Kingston-SSD/wikipedia-schools/',
        False,
        [u'wp/2/28janupdategeorg.htm', u'index.htm', u'wp/n/Niger_A.htm'],
        # u'french',
        u'computer',
        english.stop_words,
        encoding=u'utf-8',
    ),
    MAM_STARTUP=Configuration(
        u'MAM_STARTUP',
        u'/media/antharres/Kingston-SSD/mgr/mam-startup/websites-mam-startup/test1/',
        False,
        [u'mamstartup.pl/index.html'],
        u'facebook',  # good result fish and shark
        # u'naukowcy',
        # u'smartfon',
        # u'projekt',
        # u'outsourcing',
        polish.stop_words,
        encoding=u'utf-8',
    ),
    MAM_STARTUP_BIGGER=Configuration(
        u'MAM_STARTUP_BIGGER',
        u'/media/antharres/Kingston-SSD/mgr/mam-startup/websites-mam-startup-bigger/test1/',
        False,
        [u'mamstartup.pl/index.html'],
        # u'startup',
        # u'projekt',
        u'aplikac',
        # u'facebook',
        polish.stop_words,
        encoding=u'utf-8',
    ),
    DEFAULT=None
)


def set_default_config(default):
    Configurations.DEFAULT = default
    Configurations.reverse_mapping['DEFAULT'] = default


set_default_config(Configurations.MAM_STARTUP_BIGGER)
# set_default_config(Configurations.MAM_STARTUP)