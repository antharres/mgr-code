from logger import logger
from suite.util import enum


module_logger = logger.get_logger()


class CrawlingDetail(object):
    def reset(self):
        self._status = CrawlingStatus.NOT_DISCOVERED
        self._score = 0.0
        self._rank = 0.0
        self._depth = 0

    def __init__(self):
        self._status = CrawlingStatus.NOT_DISCOVERED
        self._score = 0.0
        self._rank = 0.0
        self._depth = 0

    def __str__(self):
        return u'{} - {}, {}'.format(self.__class__.__name__, self._status, self._score)

    def is_not_discovered(self):
        return self._status == CrawlingStatus.NOT_DISCOVERED

    def is_crawling_edge(self):
        return self._status == CrawlingStatus.CRAWLING_EDGE

    def is_crawled(self):
        return self._status == CrawlingStatus.CRAWLED or (
            self.__class__ == Link and self._status == CrawlingStatus.CRAWLED_SKIPPED
        )

    def mark_crawling_edge(self):
        if self.is_crawled():
            raise ValueError(u'Unexpected call mark_crawling_edge() on {} {}'.format(self._status, self.__class__.__name__))
        self._status = CrawlingStatus.CRAWLING_EDGE

    def mark_crawled(self):
        if self.is_not_discovered():
            raise ValueError(u'Unexpected call mark_crawled() on {} {}'.format(self._status, self.__class__.__name__))
        self._status = CrawlingStatus.CRAWLED

    def mark_crawled_skip(self):
        if self.__class__ is not Link:
            raise ValueError(u'mark_crawled_skip() is not allowed for {}'.format(self.__class__.__name__))
        self._status = CrawlingStatus.CRAWLED_SKIPPED

    def set_score(self, score):
        self._score = score

    def get_score(self):
        return self._score

    def set_rank(self, rank):
        self._rank = rank

    def get_rank(self):
        return self._rank

    def get_depth(self):
        return self._depth

    def set_depth(self, depth):
        self._depth = depth

    pass


CrawlingStatus = enum(
    NOT_DISCOVERED=u'NOT_DISCOVERED',
    CRAWLING_EDGE=u'CRAWLING_EDGE',
    CRAWLED=u'CRAWLED',
    CRAWLED_SKIPPED=u'CRAWLED_SKIPPED',
)


class Link(CrawlingDetail):
    def __init__(self, link_id, page_from, page_to, href, text, context):
        """
            :type link_id: int
            :type page_from: HtmlPage
            :type page_to: HtmlPage
            :type href: unicode
            :type text: unicode
            :type context: unicode
            """
        super(Link, self).__init__()
        self.link_id = link_id
        self.page_from = page_from
        self.page_to = page_to
        self.href = href
        self.text = text
        self.context = context

    def __repr__(self):
        return u'{}:\n\thref: {}\n\ttext: {}\n\tcontext: {}\n\tpage_from: {}\n\tpage_to: {}'.format(
            self.__class__.__name__,
            self.href,
            self.text,
            self.context,
            self.page_from,
            self.page_to
        )

    def __eq__(self, other):
        return other and type(other) is self.__class__ and self.link_id == other.link_id

    def __hash__(self):
        return self.link_id.__hash__()

    pass


class HtmlPage(CrawlingDetail):
    def __init__(self, url, title_text, body_text, links_from, links_to):
        """
            :type url: str
            :type title_text: str
            :type body_text: str
            """
        super(HtmlPage, self).__init__()
        self.url = url
        self.title_text = title_text
        self.body_text = body_text
        self.links_from = links_from or []
        self.links_to = links_to or []

    def __repr__(self):
        return u'{}:\n\turl: {}\n\ttitle_text: {}\n\tbody_text: {}\n\tlinks_from: {}\n\tlinks_to: {}'.format(
            self.__class__.__name__,
            self.url,
            self.title_text,
            self.body_text,
            len(self.links_from),
            len(self.links_to)
        )

    def __eq__(self, other):
        return other and type(other) is self.__class__ and self.url == other.url

    def __hash__(self):
        return self.url.__hash__()

    def reset(self):
        super(HtmlPage, self).reset()
        for link in self.links_from + self.links_to:
            link.reset()

    def mark_crawled(self):
        super(HtmlPage, self).mark_crawled()
        for link in self.links_to:
            if link.is_not_discovered():
                link.mark_crawled_skip()
            elif link.is_crawling_edge():
                link.mark_crawled()

    def get_all_discovered_inbound_pages(self):
        return map(
            lambda l: (l, l.page_from),
            filter(
                lambda l: not l.page_from.is_not_discovered(),
                self.links_to
            )
        )
    pass


class DataCorruptedException(Exception):
    def __init__(self, message):
        super(DataCorruptedException, self).__init__(message)
        pass

    pass


class CrawlingData(object):
    def __init__(self, pages):
        """
        :type pages: dict of HtmlPage
        """
        self._pages = pages

    def get_page(self, url):
        """
        :type url str
        :rtype : HtmlPage
        """
        return self._pages[url]

    def reset(self):
        for page in self._pages.values():
            page.reset()

    pass