from unittest import TestCase

from suite.model.data import HtmlPage, Link


class TestHtmlPage(TestCase):

    def test_should_return_false_for_pages_with_different_urls(self):
        page_1 = HtmlPage(u'url1', None, None, None, None)
        page_2 = HtmlPage(u'url2', None, None, None, None)
        self.assertFalse(
            page_1 == page_2
        )

    def test_should_return_true_for_pages_with_the_same_urls(self):
        page_1 = HtmlPage(u'url1', None, None, None, None)
        page_2 = HtmlPage(u'url1', None, None, None, None)
        self.assertTrue(
            page_1 == page_2
        )

    pass


class TestLink(TestCase):

    def test_should_return_false_for_links_with_different_ids(self):
        link_1 = Link(0, None, None, None, None, None)
        link_2 = Link(1, None, None, None, None, None)
        self.assertFalse(
            link_1 == link_2
        )

    def test_should_return_true_for_links_with_the_same_ids(self):
        link_1 = Link(0, None, None, None, None, None)
        link_2 = Link(0, None, None, None, None, None)
        self.assertTrue(
            link_1 == link_2
        )

    pass