try:
    import cPickle as pickle
except ImportError:
    import pickle
import io

from logger import logger
from logger.logger import LogWith
from suite import util
from suite.model.data import CrawlingData, HtmlPage, Link


module_logger = logger.get_logger()


class ImportedLink(object):
    def __init__(self, link_id, from_id, to_id, href, text, context):
        """
        :type link_id: int
        :type from_id: unicode
        :type to_id: unicode
        :type href: unicode
        :type text: unicode
        :type context: unicode
        """
        self.link_id = link_id
        self.from_id = from_id
        self.to_id = to_id
        self.href = href
        self.text = text
        self.context = context

    def __repr__(self):
        return u'{}:\n\thref: {}\n\ttext: {}\n\tfrom_id: {}\n\tto_id: {}'.format(
            self.__class__.__name__,
            self.href,
            self.text,
            self.from_id,
            self.to_id
        )

    def __eq__(self, other):
        return other and self.__class__ == other.__class__ and self.link_id == other.link_id

    def __hash__(self):
        return self.link_id.__hash__()


class ImportedHtmlPage(object):
    def __init__(self, url, html):
        """
        :type url: str
        :type html: str
        """
        self.url = url
        self.html = html
        self.links_from_ids = set()
        self.links_to_ids = set()

    def __repr__(self):
        return u'{}:\n\turl: {}\n\thtml: {}\n\tlinks_from_ids: {}\n\tlinks_to_ids: {}'.format(
            self.__class__.__name__,
            self.url,
            self.html,
            len(self.links_from_ids),
            len(self.links_to_ids)
        )

    def __eq__(self, other):
        return other and self.__class__ == other.__class__ and self.url == other.url

    def __hash__(self):
        return self.url.__hash__()


class ImportDataCorruptedException(Exception):
    def __init__(self, message):
        super(ImportDataCorruptedException, self).__init__(message)
        pass

    pass


class ImportedCrawlingData(object):
    def __init__(self):
        self.pages = {}
        self.links = {}

    @LogWith(module_logger)
    def serialize(self, data_file_name):
        module_logger.info(u'Serializing CrawlingData to {}'.format(data_file_name))
        with open(util.mkdirs(data_file_name), 'wb') as data_file:
            pickle.dump(self, data_file)

    @staticmethod
    @LogWith(module_logger)
    def deserialize(data_file_name):
        """
        :type data_file_name: str
        :rtype : CrawlingData
        """
        @LogWith(module_logger)
        def remove_unreachable_pages(pages, links):
            """
            :type pages: dict of ImportedHtmlPage
            :type links: dict of ImportedLink
            :return: dict of ImportedHtmlPage
            """
            changed = True
            i = 0
            while changed:
                removed_pages = 0
                changed = False
                i += 1
                for page in [p for url, p in pages.items() if not p.links_to_ids]:
                    removed_pages += 1
                    changed = True
                    del pages[page.url]
                    # # TODO if config only internal links....
                    for link in [links[l_id] for l_id in page.links_from_ids]:
                        del links[link.link_id]
                        page_from = pages.get(link.from_id, None)
                        if page_from:
                            page_from.links_from_ids.remove(link.link_id)
                        page_to = pages.get(link.to_id, None)
                        if page_to:
                            page_to.links_to_ids.remove(link.link_id)
                module_logger.info(u'After iteration {} - removed pages {}'.format(i, removed_pages))

        @LogWith(module_logger)
        def replace_ids(crawling_data):
            """
            :param crawling_data: ImportedCrawlingData
            :return: CrawlingData
            """
            remove_unreachable_pages(crawling_data.pages, crawling_data.links)
            for link in crawling_data.links.values():
                crawling_data.links[link.link_id] = Link(
                    link_id=link.link_id,
                    page_from=link.from_id,
                    page_to=link.to_id,
                    href=link.href,
                    text=link.text,
                    context=link.context,
                )
                del link
            new_pages = {}

            for page in crawling_data.pages.values():
                new_pages[page.url] = HtmlPage(
                    url=page.url,
                    links_from=list(crawling_data.links[link_id] for link_id in page.links_from_ids),
                    links_to=list(crawling_data.links[link_id] for link_id in page.links_to_ids),
                    title_text=page.title_text,
                    body_text=page.body_text,
                )
                del page.links_from_ids
                del page.links_to_ids
                del crawling_data.pages[page.url]
                pass

            # rewrite link pages
            for link in crawling_data.links.values():
                link.page_from = new_pages[link.page_from]
                link.page_to = new_pages.get(link.page_to, None)

            del crawling_data.links
            del crawling_data.pages

            return CrawlingData(new_pages)

        module_logger.info(u'Deserializing CrawlingData from {}'.format(data_file_name))

        with open(data_file_name, 'rb') as data_file:
            raw_data = io.BytesIO(data_file.read())
            crawling_data = pickle.load(raw_data)
            if type(crawling_data) is not ImportedCrawlingData:
                raise ImportDataCorruptedException(
                    u'Invalid type of crawling_data - expected {} but found {}'.format(ImportedCrawlingData,
                                                                                       type(crawling_data)))
            else:
                return replace_ids(crawling_data)
            pass
        pass

    pass