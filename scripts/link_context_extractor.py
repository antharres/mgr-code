# coding=utf-8
from bs4 import BeautifulSoup
import re

from logger import logger


root_logger = logger.get_logger()


class Bug(Exception):
    pass


class LinkContextExtractor(object):
    def __init__(self, context_radius):
        self.context_radius = context_radius
        self._not_empty_filter = lambda t: t
        self._split_pattern = re.compile(u'[\w]+', re.MULTILINE)

    def _index_of(self, inner, outer):
        """
        :type inner: list of unicode
        :type outer: list of unicode
        :rtype: int
        """
        inner_len = len(inner)
        outer_len = len(outer)
        skipped_offset = 0

        tmp_outer = outer[:]
        while True:
            try:
                starting_index = tmp_outer.index(inner[0])
                for i in range(inner_len):
                    outer_index = starting_index + i
                    if outer_index > outer_len:
                        # reached the end of the outer without matching inner - this must be bug
                        raise Bug(u'outer doesn\'t contain the whole inner')
                    if not tmp_outer[outer_index] == inner[i]:
                        skipped_offset += starting_index + 1
                        tmp_outer = outer[skipped_offset:]
                        # tray next
                        break
                else:
                    return starting_index + skipped_offset
            except ValueError:
                raise Bug(u'outer doesn\'t contain the whole inner\nouter = {}\ninner = {}'.format(outer, inner))

    def extract_link_context(self, link):
        parent = link.parent
        link_tokens = filter(self._not_empty_filter, self._split_pattern.findall(link.get_text(separator=u' ')))
        link_tokens_len = len(link_tokens)

        if not link_tokens:
            return u''

        while True:
            try:
                parent_tokens = filter(self._not_empty_filter, self._split_pattern.findall(parent.get_text(separator=u' ')))
                parent_tokens_len = len(parent_tokens)

                link_tokens_starting_index = self._index_of(link_tokens, parent_tokens)

                context_start_index = link_tokens_starting_index - self.context_radius
                context_end_index = link_tokens_starting_index + link_tokens_len + self.context_radius
                if type(parent) is not BeautifulSoup:
                    # reached the top - nothing more to be checked
                    if context_start_index < 0 or context_end_index > parent_tokens_len:
                        # try again with wider parent
                        parent = parent.parent
                        continue
                        # else:
                context_left = parent_tokens[context_start_index:link_tokens_starting_index]
                context_right = parent_tokens[link_tokens_starting_index + link_tokens_len:context_end_index]
                return u' '.join(context_left + context_right)
            except Exception:
                root_logger.info(u'{}\n{}'.format(link.text, parent.text))
                raise
            pass
