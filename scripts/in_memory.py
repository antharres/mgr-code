import codecs
import os
from bs4 import BeautifulSoup, Comment
import re
import gc

import urlnorm

from logger import logger
from logger.logger import LogWith
from scripts.link_context_extractor import LinkContextExtractor
from suite.model.imported import ImportedCrawlingData, ImportedLink, ImportedHtmlPage


LOG_NOISE_LIMITER = lambda i: not i % 500

root_logger = logger.get_logger()


class CrawlingDataImporter(object):
    ignored_extensions = [
        u'jpg',
        u'jpeg',
        u'gif',
        u'zip',
        u'bmp',
        u'png',
        u'css',
        u'js',
        u'txt',
        u'exe',
        u'tmp',
    ]

    def __init__(self, stop_words, encoding, include_external_links, link_context_extractor):
        """
        :type stop_words: list of unicode
        :type encoding: unicode
        :type include_external_links: bool
        :type link_context_extractor: LinkContextExtractor
        """
        self._stop_words = stop_words
        self._encoding = encoding
        self._include_external_links = include_external_links
        self.link_context_extractor = link_context_extractor

        self._crawling_data = ImportedCrawlingData()
        self._limited_mode = False
        self._split_pattern = re.compile(u'\s+')
        self._scanned_pages_counter = 0
        self._processed_pages_counter = 0
        self._links_counter = 0
        self._first_run = True

    @LogWith(root_logger)
    def build_crawling_data(self, crawled_data_root_dir):
        self._load_pages(crawled_data_root_dir)
        self._first_run = False
        self._load_pages(crawled_data_root_dir)

    @LogWith(root_logger)
    def _load_pages(self, crawled_data_root_dir):
        self._scanned_pages_counter = 0
        self._processed_pages_counter = 0
        self._links_counter = 0
        root_logger.info(u'Scanning directory {}'.format(crawled_data_root_dir))
        for dir_path, dir_names, file_names in os.walk(crawled_data_root_dir, onerror=lambda x: root_logger.warn(x)):
            for file_name in file_names:
                page = self._load_single_page(crawled_data_root_dir, file_name, dir_path)
                self._scanned_pages_counter += 1
                if page:
                    self._processed_pages_counter += 1
                    if not self._first_run:
                        self._parse_page_links(page)
            if LOG_NOISE_LIMITER(self._scanned_pages_counter):
                gc.collect()
                if self._limited_mode and self._scanned_pages_counter >= 1000:
                    return
        root_logger.info(
            u'Finished scanning.\n\tScanned {} files\n\tProcessed {} pages, saved {} pages'.format(
                self._scanned_pages_counter,
                self._processed_pages_counter,
                len(self._crawling_data.pages),
            )
        )

    def _load_single_page(self, crawled_data_root_dir, file_name, file_dir):
        full_file_name = os.path.join(file_dir, file_name)
        url = full_file_name.replace(crawled_data_root_dir, u'')
        page_disk_location = file_dir.replace(crawled_data_root_dir, u'')

        try:
            return self._load_file(full_file_name, url, page_disk_location)
        except Exception as e:
            root_logger.error(u'Error occurred when loading file {}, url {}\n\t{}'.format(full_file_name, url, e))

    def _load_file(self, full_file_name, url, page_disk_location):
        def should_process_file(file_name):
            if u'hts-' in file_name:
                return False
            for ext in CrawlingDataImporter.ignored_extensions:
                if file_name.endswith(ext):
                    return False
            return True

        def try_load_content(file_name):
            with codecs.open(file_name, u'r', self._encoding) as input_file:
                loaded_content = u'\n'.join([line for line in input_file])
                # skip empty pages
                if self._first_run and loaded_content:
                    loaded_content = u'dummy'
                return loaded_content

        page = None
        if should_process_file(full_file_name):
            if LOG_NOISE_LIMITER(self._scanned_pages_counter):
                root_logger.info(u'Loading file {}, processed {} {}'.format(self._scanned_pages_counter, self._processed_pages_counter, full_file_name))
            if u'kom_zgloszenie' not in url:
                try:
                    content = try_load_content(full_file_name).strip()
                    if content:
                        if self._first_run:
                            page = ImportedHtmlPage(url=url, html=content)
                        else:
                            page = self._crawling_data.pages[url]
                            page.html = content
                        page.page_disk_location = page_disk_location
                        self._crawling_data.pages[url] = page
                except Exception as e:
                    root_logger.warn(u"Couldn't process file {}: {} - {}".format(self._scanned_pages_counter, full_file_name, str(e)))
        return page

    # ----------------------------------------------------------------------------------------
    # links
    # ----------------------------------------------------------------------------------------

    def _parse_page_links(self, page):
        def joined_bag_of_words(text):
            text = text or u''
            return u' '.join(
                self._filter_stoplist(
                    set(self._split_pattern.split(text.lower()))
                )
            )

        def extract_links_from_page(c_soup):
            """
            :type c_soup BeautifulSoup
            """

            def handle_duplicates(links_list):
                links_map = {}
                for soup_link in links_list:
                    url = soup_link[u'href']
                    text = soup_link.text

                    (duplicate_link_text, prev_link) = links_map.get(url, (None, None))
                    if duplicate_link_text:
                        links_map[url] = (duplicate_link_text + u' ' + text, prev_link)
                        soup_link.extract().decompose()
                    else:
                        links_map[url] = (text, soup_link)
                    pass
                return links_map.items()

            result = []
            for l in c_soup.findAll(u'a', href=True):
                result.append(l)
            # result += c_soup.findAll(u'link', href=True)

            return handle_duplicates(result)

        def _compact_data_page_and_cleanup(c_page, c_soup):

            soup_title = c_soup.title
            soup_body = c_soup.body
            if not soup_body:
                root_logger.warn(u'Can\'t find body in {}'.format(c_page.url))
            c_page.title_text = joined_bag_of_words(soup_title.text if soup_title else None)
            c_page.body_text = joined_bag_of_words(soup_body.text if soup_body else c_soup.text)
            soup_title.decompose() if soup_title else None
            soup_body.decompose() if soup_body else None
            c_soup.decompose()
            del c_page.html
            del page.page_disk_location

        soup = BeautifulSoup(page.html, 'html.parser')
        # remove comments and scripts
        for x in soup.findAll('script'):
            x.extract().decompose()
        for x in soup.findAll(text=lambda text: isinstance(text, Comment)):
            x.extract()

        links = extract_links_from_page(soup)
        for (href, (link_text, soup_link)) in links:
            link_id = self._links_counter
            to_url = self._build_url(page, href.strip()).strip()

            page_to = self._crawling_data.pages.get(to_url, None)
            if self._include_external_links or page_to:
                link_context = self.link_context_extractor.extract_link_context(soup_link)
                link_context = joined_bag_of_words(link_context)
                self._crawling_data.links[link_id] = ImportedLink(
                    link_id=link_id, from_id=page.url,
                    to_id=page_to.url if page_to else None,
                    href=to_url,
                    text=link_text.strip(),
                    context=link_context,
                )

                page.links_from_ids.add(link_id)
                if page_to:
                    page_to.links_to_ids.add(link_id)

                self._links_counter += 1
                soup_link.extract().decompose()
            # else:
                # root_logger.info(u'Not found page_to {}'.format(to_url))
        _compact_data_page_and_cleanup(page, soup)

        if LOG_NOISE_LIMITER(self._scanned_pages_counter):
            root_logger.info(
                u'Extracting links {} from {}, found {} links so far'.format(self._scanned_pages_counter, page.url, self._links_counter))
            gc.collect()

    def _build_url(self, page, href):
        def try_relativize_link(href, page):
            try:
                if u'external.html?link=' not in href:
                    tmp_page_name = u'http://tmp_page_name.com/'
                    tmp_href = u'{}{}/{}'.format(tmp_page_name, page.page_disk_location, href)
                    href = urlnorm.norm(tmp_href).replace(tmp_page_name, u'')
            except UnicodeDecodeError as e:
                root_logger.warn(u'Problem occurred when parsing url {} in {}'.format(href, page.url))
            return href

        href = try_relativize_link(href, page)
        return u'{}'.format(href)

    def dump_data(self, data_file_name):
        self._crawling_data.serialize(data_file_name)
        pass

    pass

    def _filter_stoplist(self, words_set):
        return filter(lambda word: word not in self._stop_words, words_set)