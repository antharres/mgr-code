import codecs
import gc

from logger import logger
from suite import util
from suite.model.data import CrawlingData
from suite.model.imported import ImportedCrawlingData
from suite.config import Configurations


root_logger = logger.get_logger()


def _load_imported_data(config):
    """
    :rtype : CrawlingData
    """
    data = ImportedCrawlingData.deserialize(config.crawling_data_file_path)
    gc.collect()
    root_logger.info(u'Imported data:\n\t{} pages\n\t{} links_from\n\t{} links_to\n\t{} internal links_from'.format(
        len(data._pages),
        sum([len(page.links_from) for page in data._pages.values()]),
        sum([len(page.links_to) for page in data._pages.values()]),
        sum([sum((1 for page in data._pages.values() for link in page.links_from if link.page_to))])
    ))
    return data


def append_header(output_file):
    output_file.write('''<?xml version="1.0" encoding="UTF-8"?>
<gexf xmlns="http://www.gexf.net/1.2draft" version="1.2">
    <meta lastmodifieddate="2009-03-20">
        <creator>Gexf.net</creator>
        <description>A hello world! file</description>
    </meta>
    <graph mode="static" defaultedgetype="directed">
        <nodes>\n''')


def append_separator(output_file):
    output_file.write('''</nodes>
        <edges>\n''')


def append_footer(output_file):
    output_file.write('''</edges>
    </graph>
</gexf>''')


def main():
    config = Configurations.DEFAULT
    root_logger.info(u'Using Configuration {}'.format(config))

    data = _load_imported_data(config)

    with codecs.open(util.mkdirs(config.gephx_file_path), 'w', encoding='utf-8') as output_file:
        append_header(output_file)

        page_id = 0
        for page in data._pages.values():
            page.id = page_id
            page_id += 1
            output_file.write(u'\t\t<node id="{}" label="{}" />\n'.format(page.id, page.url))
        append_separator(output_file)
        for link in [link for page in data._pages.values() for link in page.links_from if link.page_to]:
            output_file.write(u'\t\t<edge source="{}" target="{}" />\n'.format(link.page_from.id, link.page_to.id))

        append_footer(output_file)


if __name__ == '__main__':
    main()