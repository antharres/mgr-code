import gc

from logger import logger
from logger.logger import LogWith
from suite.crawler.server.score.evaluator import Evaluator
from suite.crawler.server.strategy.page_rank import page_rank
from suite.model.data import HtmlPage, Link
from suite.model.imported import ImportedCrawlingData
from suite.config import Configurations


root_logger = logger.get_logger()


def _load_imported_data(config):
    """
    :type : CrawlingData
    """
    data = ImportedCrawlingData.deserialize(config.crawling_data_file_path)
    gc.collect()
    root_logger.info(u'Imported data:\n\t{} pages\n\t{} links_from\n\t{} links_to\n\t{} internal links_from'.format(
        len(data._pages),
        sum([len(page.links_from) for page in data._pages.values()]),
        sum([len(page.links_to) for page in data._pages.values()]),
        sum([sum((1 for page in data._pages.values() for link in page.links_from if link.page_to))])
    ))
    return data


@LogWith(root_logger)
def count_word_occurrences(crawling_data):
    """
    :type : CrawlingData
    """
    word_occurrences = {}
    for page in crawling_data._pages.values():
        for word in page.body_text.split(u' '):
            current = word_occurrences.get(word, 0)
            word_occurrences[word] = current + 1
    return word_occurrences


def n_most_common_words(crawling_data, n, max_occurrences_ratio):
    """
    :type : CrawlingData
    :type n int
    :type max_occurrences_ratio float
    """
    word_occurences = count_word_occurrences(crawling_data)
    words = u''
    max_occurrences_threshold = len(crawling_data._pages) * max_occurrences_ratio
    for word in filter(lambda w: word_occurences[w] < max_occurrences_threshold,
                       sorted(word_occurences, key=word_occurences.get, reverse=True))[:n]:
        words += u'\t{} - {}\n'.format(word, word_occurences[word])
    root_logger.info(u'Top {} words with less than {} occurrences:\n{}'.format(n, max_occurrences_threshold, words))
    pass


def unreachable_pages_stats(data):
    stats = u'Unreachable pages: {}\n\tlinks: count\n'
    unreachable_pages = [page for page in data._pages.values() if not page.links_to]
    stats.format(len(unreachable_pages))
    links_from_stats = {}
    for page in unreachable_pages:
        links_from_stats[len(page.links_from)] = links_from_stats.get(len(page.links_from), 0) + 1
    for key in sorted(links_from_stats, reverse=True):
        stats += u'\t{}: {}\n'.format(key, links_from_stats[key])
    root_logger.info(stats)


@LogWith(root_logger)
def analyze_number_of_components(data):
    """
    :param data: CrawlingData
    :return:
    """
    links = []
    components = {}
    i = 0
    for page in data._pages.values():
        page.component = i
        i += 1
        links.extend(page.links_from)
        l = components.get(page.component, [])
        components[page.component] = l
        l.append(page)
    assert len(data._pages) == len(components)
    for k, v in components.items():
        try:
            assert type(k) is int and type(v) is list
        except AssertionError:
            root_logger.info(u'{}, {}'.format(type(k), type(v)))
            raise
    for link in links:
        from_component = link.page_from.component
        to_component = link.page_to.component
        if from_component is not to_component:
            # join
            f = components[from_component]
            for p in components[to_component]:
                p.component = from_component
                f.append(p)
            del components[to_component]
            pass
    root_logger.info(u'Number of components: {}'.format(len(components)))
    for id, l in components.items():
        root_logger.info(u'Component {}, len {}, page {}'.format(id, len(l), l[0].url))
    pass


def analyze_degrees_stats(data):
    in_degrees = {}
    out_degrees = {}
    for page in data._pages.values():
        key = len(page.links_from)
        out_degrees[key] = out_degrees.get(key, 0) + 1

        key = len(page.links_to)
        in_degrees[key] = in_degrees.get(key, 0) + 1
    print u'out_degrees = [{}]'.format(u', '.join(unicode(out_degrees[k]) for k in sorted(out_degrees)))
    print u'out_degrees_x = [{}]'.format(u', '.join(unicode(k) for k in sorted(out_degrees)))
    print u'in_degrees = [{}]'.format(u', '.join(unicode(in_degrees[k]) for k in sorted(in_degrees)))
    print u'in_degrees_x = [{}]'.format(u', '.join(unicode(k) for k in sorted(in_degrees)))


def log_matching_links_stats(config, data):
    evaluator = Evaluator(config.query)
    root_logger.info(u'-' * 120)
    root_logger.info(
        u'# links matching query {}: {}'.format(
            config.query,
            sum(
                [1 for p in data._pages.values() for link in p.links_from if evaluator.evaluate_link_score(link)]
            )
        )
    )
    root_logger.info(
        u'# pages with links_to matching query {}: {}'.format(
            config.query,
            sum(
                [1 for p in data._pages.values() if
                 sum([1 for link in p.links_to if evaluator.evaluate_link_score(link)])]
            )
        )
    )
    root_logger.info(
        u'# pages with links_to matching query if page matches the query {}: {}'.format(
            config.query,
            sum(
                [1 for p in data._pages.values() if (evaluator.evaluate_page_score(p) and sum(
                    [1 for link in p.links_to if evaluator.evaluate_link_score(link)]))]
            )
        )
    )

    root_logger.info(u'-' * 120)

    root_logger.info(
        u'# links and contexts matching query {}: {}'.format(
            config.query,
            sum(
                [1 for p in data._pages.values() for link in p.links_from if
                 evaluator.evaluate_link_score(link) or evaluator.evaluate_link_context_score(link)]
            )
        )
    )
    root_logger.info(
        u'# pages with links_to and contexts matching query {}: {}'.format(
            config.query,
            sum(
                [1 for p in data._pages.values() if
                 sum([1 for link in p.links_to if
                      evaluator.evaluate_link_score(link) or evaluator.evaluate_link_context_score(link)])]
            )
        )
    )
    root_logger.info(
        u'# pages with links_to and contexts matching query if page matches the query {}: {}'.format(
            config.query,
            sum(
                [1 for p in data._pages.values() if (evaluator.evaluate_page_score(p) and sum(
                    [1 for link in p.links_to if
                     evaluator.evaluate_link_score(link) or evaluator.evaluate_link_context_score(link)]))]
            )
        )
    )

    root_logger.info(u'-' * 120)

    root_logger.info(
        u'# pages matching query {}: {}'.format(
            config.query,
            sum(
                [1 for p in data._pages.values() if evaluator.evaluate_page_score(p)]
            )
        )
    )
    root_logger.info(u'-' * 120)


@LogWith(root_logger)
def main():
    config = Configurations.DEFAULT
    root_logger.info(u'Using Configuration {}'.format(config))

    data = _load_imported_data(config)
    raw_input(u'Press any key')
    root_logger.info(u'Page urls:')
    for i in set((url[:13] for url in data._pages.keys())):
        root_logger.info(u'\t{}'.format(i))
    pass
    root_logger.info(u'External links:')
    for i in set((link.href[:10] for page in data._pages.values() for link in page.links_from if not link.page_to)):
        root_logger.info(u'\t{}'.format(i))
    pass

    root_logger.info(u'Pages with invalid class:')
    for page in [page for page in data._pages.values() if type(page) is not HtmlPage]:
        root_logger.warn(u'{}'.format(type(page)))
        pass
    root_logger.info(u'Links with invalid class:')
    for page in [link for page in data._pages.values() for link in page.links_from + page.links_to if
                 type(link) is not Link]:
        root_logger.warn(u'{}'.format(type(page)))
        pass
    unreachable_pages_stats(data)
    n_most_common_words(crawling_data=data, n=50, max_occurrences_ratio=0.5)

    analyze_degrees_stats(data)

    analyze_number_of_components(data)
    for page in data._pages.values():
        page.mark_crawling_edge()
    page_rank(data._pages)

    log_matching_links_stats(config, data)


if __name__ == '__main__':
    main()