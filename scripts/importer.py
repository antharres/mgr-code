from logger import logger
from logger.logger import LogWith
from scripts.in_memory import CrawlingDataImporter
from scripts.link_context_extractor import LinkContextExtractor
from suite.config import Configurations


root_logger = logger.get_logger()


@LogWith(root_logger)
def _import_data(config):
    """
    :param config: Configuration
    """
    link_context_extractor = LinkContextExtractor(config.link_context_radius)
    importer = CrawlingDataImporter(
        config.stop_words,
        config.encoding,
        config.include_external_links,
        link_context_extractor
    )
    importer.build_crawling_data(config.data_root_dir)
    importer.dump_data(config.crawling_data_file_path)
    pass


def main():
    config = Configurations.DEFAULT
    root_logger.info(u'Using Configuration {}'.format(config))

    _import_data(config)


if __name__ == '__main__':
    main()