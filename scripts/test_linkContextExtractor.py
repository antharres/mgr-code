# coding=utf-8
from bs4 import BeautifulSoup
from unittest import TestCase

from scripts.link_context_extractor import LinkContextExtractor


html_doc = u'''<html>
  <head>
   <title>
    The Dormouse's story
   </title>
  </head>
  <body>
   <p class="title">
    <b>
     The Dormouse's story (Elsie)
    </b>
   </p>
   <p class="story">
    Once upon a time there were three little sisters; and their names were ąęółśćźż
    <a class="sister" href="http://example.com/elsie" id="link1">
     Elsie and ąęółśćźż
    </a> ąęółśćźż
    and they lived at the bottom of a well.
   </p>
   <p class="story">
   qwerty
    ...
   </p>
   <!-- test comment -->
   <script>
    x = document.getById('te');
    </script>
  </body>
 </html>'''


class TestLinkContextExtractor(TestCase):
    def test_extract_link_context_with_0_radius(self):
        self._extractor = LinkContextExtractor(0)
        link = BeautifulSoup(html_doc).find(id="link1")
        self.assertIsNotNone(link)
        self.assertEqual(u'', self._extractor.extract_link_context(link))

    def test_extract_link_context_with_3_radius(self):
        self._extractor = LinkContextExtractor(3)
        soup = BeautifulSoup(html_doc)
        print soup.get_text(separator=u' ')
        link = soup.find(id="link1")
        self.assertIsNotNone(link)
        context = self._extractor.extract_link_context(link)
        self.assertEqual(u'their names were and they lived', context)

    def test_extract_link_context_with_10_radius_should_go_up_the_tree(self):
        self._extractor = LinkContextExtractor(10)
        link = BeautifulSoup(html_doc).find(id="link1")
        self.assertIsNotNone(link)
        context = self._extractor.extract_link_context(link)
        self.assertEqual(
            u'time there were three little sisters and their names were and they lived at the bottom of a well qwerty',
            context
        )

    def test_extract_link_context_with_100_radius_should_go_up_the_tree_and_stop_at_the_root(self):
        self._extractor = LinkContextExtractor(100)
        link = BeautifulSoup(html_doc).find(id="link1")
        self.assertIsNotNone(link)
        context = self._extractor.extract_link_context(link)
        self.assertEqual(
            u'The Dormouse s story The Dormouse s story Elsie Once upon a time there were three little sisters and their names were and they lived at the bottom of a well qwerty x document getById te',
            context
        )