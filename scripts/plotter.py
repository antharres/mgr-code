# coding=utf-8
import codecs
import os
import matplotlib.pyplot as plt
import re

import numpy

from logger import logger
from suite.config import Configurations


module_logger = logger.get_logger()


def plot_graph(config, title, output_file_name, data, label_x, label_y, get_y_values_function, transform_y,
               additional_plot_settings, legend_loc):
    plt.xlabel(label_x)
    plt.ylabel(label_y)
    plt.title(title)

    for crawling_stats in data.values():
        module_logger.info(u'-' * 50)
        module_logger.info(u'Processing {} - {}'.format(crawling_stats.name, title))
        y = list([transform_y(crawling_stats.pages_crawled[i], get_y_values_function(crawling_stats)[i]) for i in
                  xrange(len(get_y_values_function(crawling_stats)))])
        plt.plot(
            crawling_stats.pages_crawled,
            y,
            label=crawling_stats.name
        )
        module_logger.info(u'min: {}, max: {}, avg: {}, median: {}'.format(min(y), max(y), sum(y) / len(y),
                                                                           numpy.median(numpy.array(y))))
        # plt.xlim(xmin=0, xmax=max(crawling_stats.pages_crawled))

    plt.legend(loc=legend_loc, ncol=1, fancybox=True, shadow=True)
    additional_plot_settings(plt)

    fig = plt.gcf()
    plt.show()

    save_dir = u'{}plots/'.format(config.stats_dir_path)
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    file_name = re.sub(u'\s+', u'_', u'{}{}.png'.format(save_dir, output_file_name))
    module_logger.info(u'Saving image {}'.format(file_name))
    fig.savefig(file_name, bbox_inches='tight')


def precision(x, y):
    return 0.0 if x == 0 else y * 1.0 / x


def recall(matching_pages_count, y):
    return y * 1.0 / matching_pages_count


def main():
    config = Configurations.DEFAULT

    stats_data = load_data(config)

    matching_pages_count = max(x.matching_pages_count for x in stats_data.values())
    all_pages_count = max(x.all_pages_count for x in stats_data.values())

    def limit_y_matching_pages_count(plt):
        plt.ylim(ymax=float(matching_pages_count))

    def limit_y_all_pages_count(plt):
        plt.axhline(y=matching_pages_count, color='r', linestyle='-.')
        plt.ylim(ymax=float(all_pages_count))

    matching_pages_count = max(x.matching_pages_count for x in stats_data.values())

    def get_matching_pages_crawled(crawling_stats):
        return crawling_stats.matching_pages_crawled

    def get_crawling_edge_size(crawling_stats):
        return crawling_stats.crawling_edge_size

    def harvest_rate_modifications(plt):
        # plt.ylim(ymin=0.0, ymax=1.0)
        matching_pages_ratio = matching_pages_count * 1.0 / all_pages_count
        plt.axhline(y=matching_pages_ratio, color='black', linestyle='--')

    def f1_score_modifications(plt):
        # plt.ylim(ymax=float(1.0))
        pass

    def f1_score(x, y):
        prec_v = precision(x, y)
        rec_v = recall(matching_pages_count, y)
        try:
            return 2 * prec_v * rec_v / (prec_v + rec_v)
        except ZeroDivisionError:
            return 0.0

    def percentage(x, y):
        y = recall(matching_pages_count, y) * 100
        if abs(80 - y) < .05:
            module_logger.info(u'\t{}: {}'.format(x, y))
        return y

    plots = [
        (
            u'Moment odkrycia n interesujących stron',
            u'n_discovery_time',
            u'Liczba odwiedzonych stron',
            u'Liczba interesujących stron',
            get_matching_pages_crawled,
            lambda x, y: y,
            limit_y_matching_pages_count,
            'lower right',
        ),
        # (
        #     'Crawled all pages',
        #     'Number of downloaded pages',
        #     'Number of matching pages',
        #     get_matching_pages_crawled,
        #     lambda x, y: y,
        #     limit_y_all_pages_count,
        #     'upper right',
        # ),
        # (
        #     u'Moment odkrycia n% interesujących stron',
        #     u'Liczba odwiedzonych stron',
        #     u'% odkrytych interesujących stron',
        #     get_matching_pages_crawled,
        #     percentage,
        #     lambda plt: plt.ylim(ymax=float(100.0)),
        #     'lower right',
        # ),
        (
            u'Rozmiar kolejki odnośników',
            u'crawling_edge_size',
            u'Liczba odwiedzonych stron',
            u'Rozmiar kolejki odnośników',
            get_crawling_edge_size,
            lambda x, y: y,
            lambda plt: plt,
            'upper right',
        ),
        (
            u'Zbieżność tematyczna',
            u'harvest_rate',
            u'Liczba odwiedzonych stron',
            u'Zbieżność tematyczna',
            get_matching_pages_crawled,
            precision,
            harvest_rate_modifications,
            'upper right',
        ),
        # (
        #     'F-1 score',
        #     'Number of downloaded pages',
        #     'Percent of matching pages',
        #     get_matching_pages_crawled,
        #     f1_score,
        #     f1_score_modifications,
        #     'upper right',
        # )
    ]
    for (
            title,
            output_file_name,
            label_x,
            label_y,
            get_y_values_function,
            transform_y,
            additional_plot_settings,
            legend_loc
    ) in plots:
        module_logger.info(u'-' * 120)
        plot_graph(
            config,
            u'{}\nsłowo kluczowe {}'.format(title, config.query),
            output_file_name,
            stats_data,
            label_x,
            label_y,
            get_y_values_function,
            transform_y,
            additional_plot_settings,
            legend_loc
        )
        module_logger.info(u'-' * 50)


class CrawlingStats(object):
    # 0: 0 0 1.000000 index.html
    stats_line_pattern = re.compile(
        u'^(?P<x>\d+):\s+(?P<y>\d+)\s+(?P<crawling_edge_size>\d+)\s+(?P<score>\d+\.\d+)\s+(?P<url>.+)$')
    # Matching pages count: {:d}
    matching_pages_count_line_pattern = re.compile(u'^Matching pages count: (?P<matching_pages_count>\d+)$')
    # All pages count: {:d}
    all_pages_count_line_pattern = re.compile(u'^All pages count: (?P<all_pages_count>\d+)$')

    def __init__(self, name):
        self.name = name
        self.matching_pages_count = 0
        self.all_pages_count = 0
        self.pages_crawled = []
        self.matching_pages_crawled = []
        self.crawling_edge_size = []

    def feed_with_line(self, line):
        for f in [
            self._feed_with_log_line,
            self._feed_with_line_all_pages_count,
            self._feed_with_line_matching_pages_count
        ]:
            if f(line): return
        else:
            module_logger.warn(u'{} - unknown line - {}'.format(self.name, line))

    def _feed_with_log_line(self, line):
        m = CrawlingStats.stats_line_pattern.match(line)
        if m:
            self.pages_crawled.append(int(m.group(u'x')))
            self.matching_pages_crawled.append(float(m.group(u'y')))
            self.crawling_edge_size.append(float(m.group(u'crawling_edge_size')))
            return True
        else:
            return False

    def _feed_with_line_matching_pages_count(self, line):
        m = CrawlingStats.matching_pages_count_line_pattern.match(line)
        if m:
            self.matching_pages_count = int(m.group(u'matching_pages_count'))
            return True
        else:
            return False

    def _feed_with_line_all_pages_count(self, line):
        m = CrawlingStats.all_pages_count_line_pattern.match(line)
        if m:
            self.all_pages_count = int(m.group(u'all_pages_count'))
            return True
        else:
            return False

    pass


def load_data(config):
    result = {}
    module_logger.info(u'Scanning directory {}'.format(config.stats_dir_path))
    for (dir_path, dir_names, file_names) in os.walk(config.stats_dir_path):
        for file_name in file_names:
            if u'plots' not in dir_path:
                with codecs.open(os.path.join(dir_path, file_name)) as stats_file:
                    module_logger.info(u'Scanning stats file {}'.format(stats_file.name))
                    lines = stats_file.readlines()
                    title = lines[0].strip().replace(u'Strategy', u'')
                    data = CrawlingStats(title)
                    result[stats_file.name] = data
                    for line in lines[1:]:
                        data.feed_with_line(line.strip())
    return result


if __name__ == '__main__':
    main()
